$(document).ready(function() {
	/** Mobile Responsive Settings **/
	var winW = $(window).width();
	var device = 0;
	if(winW > 992 && winW  < 1200) {
		device = 1;
	} else if(winW < 992) {
		device = 2;
	}
	var blogItem = 4;
	var nowOpen = 0;
	var eventItem = 3;
	if(device == 0){
		nowOpen = 225;
	} else if(device == 1) {
		nowOpen = 100;
	} else if(device == 2) {
		blogItem = 2;
		eventItem = 2;
	}
	/** end of Mobile Responsive Settings **/

	/** Init **/
	$(".dot").dotdotdot({
		watch: window
	});
	/** end of Init **/

	/** Menu **/
	$('#menu').affix({
		offset: {
			top: function () {
				return $('#home').outerHeight(true);
			}
		}
	});
	$(window).scroll(function (event) {
		var scroll = $(window).scrollTop();
		var iheight = $('#home').innerHeight();
		var sheight = this.scrollHeight;
		if(scroll>100){
			$('.sub-menu').addClass('bot');
		}else{
			$('.sub-menu').removeClass('bot');
		}
	});
	$('.menu-bar').click(function(){
		var barhas = $('.mobile-menu').hasClass('active');
		if(barhas){
			$('.mobile-menu').removeClass('active');
		}else{
			$('.mobile-menu').addClass('active');
		}
	});
	$('.mobile-menu li:not(.wo):not(.search)').click(function(){
		$('.mobile-menu').removeClass('active');
	});
	$('.wo').click(function(){
		if($('.wo .sub-menu').hasClass('active')){
			$('.wo .sub-menu').removeClass('active');
		}else{
			$('.wo .sub-menu').addClass('active');
		}
	});
	$('#menu li a').click(function(){
		var value = $(this).attr('href');
		$('html, body').animate({
			'scrollTop': $(value).offset().top-50
		});
	});
	$('.mobile-menu li a').click(function(){
		var value = $(this).attr('href');
		$('html, body').animate({
			'scrollTop': $(value).offset().top-50
		});
	});
	/** end of Menu **/

	/** Banner Section **/
	$('.versatile-slider').owlCarousel({
		items:1,
		loop:false
		// autoplay:true
	});
	/** end of Banner Section **/

	/** Mini Gallery Section **/
	$('.mini-gallery-slider').owlCarousel({
		items:blogItem,
		loop:false,
		margin:10,
		dots:true,
		dotsEach:true
	});
	$('.mini-gallery-details-slider').owlCarousel({
		items:blogItem,
		margin:10,
		nav:true,
		navText: ['<div class="banner-nav prev"><span class="fa fa-angle-left"></span></div>','<div class="banner-nav next"><span class="fa fa-angle-right"></span></div>']
	});
	$('.mini-gallery-slider .see-more').click(function(){
		var title = $(this).parent().children('.title').text();
		var description = $(this).parent().children('.content-full').html();
		var data = $(this).data('blogimages');
		var images = data.split('>');
		var htmldata = '';
		var i = 0;
		$('.mini-gallery-details-slider').trigger('destroy.owl.carousel');
		$('.mini-gallery-details-slider').removeClass('owl-carousel owl-loaded');
		$('.mini-gallery-details-slider').find('.owl-stage-outer').children().unwrap();
		images.forEach(function(value){
			if(i!=0){
				htmldata+= '<div class="item" style="background-image:url(./res/uploads/'+value+');background-size:100% 100%;"><img src="./res/uploads/'+value+'" alt="Plaza Indonesia Blog" style="opacity:0;"></div>';
			}
			i++;
		});
		$('.mini-gallery-details-slider').html(htmldata);
		$('.mini-gallery-details-wrapper').addClass('active');
		$('.mini-gallery-details-slider').addClass('owl-carousel');
		$('.mini-gallery-details-slider').owlCarousel({
			items:blogItem,
			margin:10,
			nav:true,
			navText: ['<div class="banner-nav prev"><span class="fa fa-angle-left"></span></div>','<div class="banner-nav next"><span class="fa fa-angle-right"></span></div>']
		});
		$('.mini-gallery-details .title').text(title);
		$('.mini-gallery-details .content').html(description);
	});
	$('.close-mini-gallery').click(function(){
		$('.mini-gallery-details-wrapper').removeClass('active');
	});
	/** end of Mini Gallery Section **/
});