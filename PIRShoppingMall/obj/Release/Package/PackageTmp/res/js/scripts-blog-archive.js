$(document).ready(function() {
	/** Mobile Responsive Settings **/
	var winW = $(window).width();
	var device = 0;
	if(winW > 992 && winW  < 1200) {
		device = 1;
	} else if(winW < 992) {
		device = 2;
	}
	var blogItem = 4;
	var nowOpen = 0;
	var eventItem = 3;
	if(device == 0){
		nowOpen = 225;
	} else if(device == 1) {
		nowOpen = 100;
	} else if(device == 2) {
		blogItem = 2;
		eventItem = 2;
	}
	/** end of Mobile Responsive Settings **/

	/** Init **/
	$(".dot").dotdotdot({
		watch: window
	});
	/** end of Init **/

	/** Menu **/
	
	/** end of Menu **/

	/** Blog Archive Section **/
	$('.blog-slider .see-more').click(function(){
		var title = $(this).parent().children('.title').text();
		var description = $(this).parent().children('.content-full').html();
		var data = $(this).data('blogimages');
		var images = data.split('>');
		var htmldata = '';
		var i = 0;
		var winW = $(window).width();
		var device = 0;
		var sliderItem = 4;
		if(winW > 992 && winW  < 1200){
			device = 1;
		}else if(winW < 992){
			device = 2;
		}
		if(device==2){
			sliderItem = 2;
		}
		$('.blog-details-slider').trigger('destroy.owl.carousel');
		$('.blog-details-slider').removeClass('owl-loaded');
		$('.blog-details-slider').find('.owl-stage-outer').children().unwrap();
		images.forEach(function(value){
			if(i!=0){
			    htmldata += '<div class="item" style="background-image:url(' + value + ');background-size:100% 100%;"><img src="' + value + '" alt="Plaza Indonesia Blog" style="opacity:0;"></div>';
			}
			i++;
		});
		$('.blog-details-slider').html(htmldata);
		$('.blog-details-wrapper').addClass('active');
		$('.blog-details-slider').owlCarousel({
			items:sliderItem,
			margin:10,
			nav:true,
			navText: ['<div class="banner-nav prev"><span class="fa fa-angle-left"></span></div>','<div class="banner-nav next"><span class="fa fa-angle-right"></span></div>']
		});
		$('.blog-details .title').text(title);
		$('.blog-details .content').html(description);
	});
	$('.close-blog').click(function(){
		$('.blog-details-wrapper').removeClass('active');
	});
	/** end of Banner Section **/
});