$(document).ready(function() {
	/** Mobile Responsive Settings **/
	var winW = $(window).width();
	var device = 0;
	if(winW > 992 && winW  < 1200) {
		device = 1;
	} else if(winW < 992) {
		device = 2;
	}
	var blogItem = 4;
	var nowOpen = 0;
	var eventItem = 3;
	if(device == 0){
		nowOpen = 225;
	} else if(device == 1) {
		nowOpen = 100;
	} else if(device == 2) {
		blogItem = 2;
		eventItem = 2;
	}
	/** end of Mobile Responsive Settings **/

	/** Init **/
	$(".dot").dotdotdot({
		watch: window
	});
	/** end of Init **/

	/** Menu **/
	$('#menu').affix({
		offset: {
			top: function () {
				return $('#home').outerHeight(true);
			}
		}
	});
	$(window).scroll(function (event) {
		var scroll = $(window).scrollTop();
		var iheight = $('#home').innerHeight();
		var sheight = this.scrollHeight;
		if(scroll>100){
			$('.sub-menu').addClass('bot');
		}else{
			$('.sub-menu').removeClass('bot');
		}
	});
	$('.menu-bar').click(function(){
		var barhas = $('.mobile-menu').hasClass('active');
		if(barhas){
			$('.mobile-menu').removeClass('active');
		}else{
			$('.mobile-menu').addClass('active');
		}
	});
	$('.mobile-menu li:not(.wo):not(.search)').click(function(){
		$('.mobile-menu').removeClass('active');
	});
	$('.wo').click(function(){
		if($('.wo .sub-menu').hasClass('active')){
			$('.wo .sub-menu').removeClass('active');
		}else{
			$('.wo .sub-menu').addClass('active');
		}
	});
	$('#menu li a').click(function(){
		var value = $(this).attr('href');
		$('html, body').animate({
			'scrollTop': $(value).offset().top-50
		});
	});
	$('.mobile-menu li a').click(function(){
		var value = $(this).attr('href');
		$('html, body').animate({
			'scrollTop': $(value).offset().top-50
		});
	});
	/** end of Menu **/

	/** Promo Info Section **/
	$('.promo-slider').owlCarousel({
		items:1,
		loop:false,
		nav:true,
		navText: ['<div class="banner-nav prev"><span class="fa fa-angle-left"></span></div>','<div class="banner-nav next"><span class="fa fa-angle-right"></span></div>']
	});
	/** end of Promo Info Section **/
});