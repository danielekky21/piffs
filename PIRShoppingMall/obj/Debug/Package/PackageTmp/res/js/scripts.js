/** Google Recaptcha **/
var myCallBack = function() {
	recaptcha1 = grecaptcha.render('recaptcha1', {
		'sitekey' : '6LdhywgTAAAAAPsSgQCiSB-liEVtL3jiuA5j2KY9',
	});
	recaptcha2 = grecaptcha.render('recaptcha2', {
		'sitekey' : '6LdhywgTAAAAAPsSgQCiSB-liEVtL3jiuA5j2KY9',
	});
};
/** end of Google Recaptcha **/

$(document).ready(function() {
	/** Mobile Responsive Settings **/

	var winW = $(window).width();
	var device = 0;
	if(winW > 992 && winW  < 1200) {
		device = 1;
	} else if(winW < 992) {
		device = 2;
	}
	var blogItem = 4;
	var nowOpen = 0;
	var eventItem = 3;
	if(device == 0){
		nowOpen = 225;
	} else if(device == 1) {
		nowOpen = 100;
	} else if(device == 2) {
		blogItem = 2;
		eventItem = 2;
	}
	/** end of Mobile Responsive Settings **/

	/** Init **/
	$(".dot").dotdotdot({
		watch: window
	});
	/** end of Init **/

	/** Menu **/
	$('#menu').affix({
		offset: {
			top: function () {
                return $('#menu').outerHeight(true);
			}
		}
	});
	$(window).scroll(function (event) {
		var scroll = $(window).scrollTop();
		var iheight = $('#home').innerHeight();
		var sheight = this.scrollHeight;
		if(scroll>100){
			$('.sub-menu').addClass('bot');
		}else{
			$('.sub-menu').removeClass('bot');
		}
	});
	$('.menu-bar').click(function(){
		var barhas = $('.mobile-menu').hasClass('active');
		if(barhas){
			$('.mobile-menu').removeClass('active');
		}else{
			$('.mobile-menu').addClass('active');
		}
	});
	$('.mmenu').click(function(){
		var mmenu = $('.mobile-menu').hasClass('active');
		if(mmenu){
			$('.mobile-menu').removeClass('active');
		}else{
			$('.mobile-menu').addClass('active');
		}
	});
	$('.mobile-menu li:not(.wo):not(.search)').click(function(){
		$('.mobile-menu').removeClass('active');
	});
	$('.wo').click(function(){
		if($('.wo .sub-menu').hasClass('active')){
			$('.wo .sub-menu').removeClass('active');
		}else{
			$('.wo .sub-menu').addClass('active');
		}
	});
	$('#menu li a').click(function(){
		var value = $(this).attr('href');
		$('html, body').animate({
			'scrollTop': $(value).offset().top-50
		});
	});
	$('.mobile-menu li a').click(function(){
		var value = $(this).attr('href');
		$('html, body').animate({
			'scrollTop': $(value).offset().top-50
		});
	});
	/** end of Menu **/
    $('.home-slider-modal-button').on('click', function () {
        var $this = $(this);
        var target = $this.data('target');
        if (target !== undefined) {
            var $banner = $('.home-slider .owl-item:not(cloned) .item.has-popup[data-target="' + target + '"]');
            var bannerIndex = $banner.data('index') - 1;
            $('.home-slider').trigger('to.owl.carousel', [bannerIndex, 300]);

            $('.home-slider-modal').modal('hide');
            $('.home-slider-modal').on('hidden.bs.modal', function () {
                $(target).modal('show');
                $('.home-slider-modal').off('hidden.bs.modal');
                $('.home-slider-modal').on('hidden.bs.modal', function () {
                    $('body').toggleClass('home-slider-modal-open');
                });
                $('.home-slider-modal').on('hidden.bs.modal', function () {
                    $('body').toggleClass('fixed-header');
                });
                $('body').addClass('fixed-header');
                $('body').addClass('home-slider-modal-open');
            });
        }
    });

	/** Banner Section **/
	$('.home-slider').owlCarousel({
		items:1,
		loop:true,
		// autoplay:true,
		nav:true,
		navText: ['<div class="banner-nav prev"><span class="fa fa-angle-left"></span></div>','<div class="banner-nav next"><span class="fa fa-angle-right"></span></div>']
	});
	$('.home-slider-modal').on('show.bs.modal hidden.bs.modal',function() {
		$('body').toggleClass('home-slider-modal-open');
	});
	$('.home-slider-modal').on('show.bs.modal hidden.bs.modal',function() {
		$('body').toggleClass('fixed-header');
	});
	/** end of Banner Section **/

	/** What's Happening Section **/
	var whatsHappeningHold = false;
	var whatsHappeningHoldTimer;
	$('.whats-happening-item').click(function(){
		if(device < 2) {
			if(!whatsHappeningHold){
				var $this = $(this);
				var segment = $this.data('segment');

				whatsHappeningHold = true;
				$('.whats-happening-item').not(this).addClass('not-active');
				$(this).addClass('active');
				clearTimeout(whatsHappeningHoldTimer);
				whatsHappeningHoldTimer = setTimeout(function(){
					whatsHappeningHold = false;
					if(segment == 'nowopen') {
						var $sliderItem = $('.whno-slider .owl-item.active .wh-slider-item');
						var name = $sliderItem.data('title');
						var level = $sliderItem.data('level');
						var desc = $sliderItem.data('desc');

						var $sidebar = $('.whats-happening-sidebar.nowopen .whats-happening-sidebar-content');
						$sidebar.children('.whno-title').text(name);
						$sidebar.children('.whno-level').text(level);
						$sidebar.children('.whno-desc').text(desc);
						$sidebar.children('.whno-title').fadeIn();
						$sidebar.children('.whno-level').fadeIn();
						$sidebar.children('.whno-desc').fadeIn();
					}
				}, 1000);
			}
		} else {
			var $this = $(this);
			var modalTarget = $this.data('modal-target');
			$('#' + modalTarget).modal('show');
			$('#wh-events-modal').on('shown.bs.modal', function () {
				setTimeout(function() {
					$('.wh-events-modal-body .wh-slider').addClass('show');
				},500);
			});
			$('#wh-nowopen-modal').on('shown.bs.modal', function () {
				var $sliderItem = $('.whno-modal-slider .owl-item:first-child .wh-slider-item');
				var name = $sliderItem.data('title');
				var level = $sliderItem.data('level');
				var desc = $sliderItem.data('desc');

				var $modalHeader = $('.whno-modal-header');
				$modalHeader.children('.whno-title').text(name);
				$modalHeader.children('.whno-level').text(level);
				$modalHeader.children('.whno-desc').text(desc);
				setTimeout(function() {
					$('.wh-nowopen-modal-body .wh-slider').addClass('show');
				},500);
			});
			$('.whp-modal-item').on('click',function() {
				var $this = $(this);
				var title = $this.html();
				var slider = $this.data('slider');
				var $zoom = $this.parent().parent().children('.zoom-button');

				$('.whp-modal-list').removeClass('show');
				$('.whp-modal-list-header').html(title);
				setTimeout(function() {
					$('.whp-modal-list').addClass('hidden');
					$('.whp-modal-slider').addClass('hidden');
					$('.whp-modal-back').removeClass('hidden');
					$('.' + slider).removeClass('hidden');
					setTimeout(function() {
						$('.whp-modal-list-header').addClass('show');
						$('.' + slider).addClass('show');
						var $sliderItem = $('.' + slider).find('.owl-item.active .wh-slider-item');
						var $slideritemImg = $sliderItem.children('img');
						$('.' + slider).parent().children('.zoom-button').attr('data-image',$slideritemImg.attr('src'));
						$zoom.addClass('active');
					},500);
				},300);
			});
			$('.whp-modal-back').on('click',function(){
				var $this = $(this);
				$this.parent().find('.zoom-button').removeClass('active');

				$this.addClass('hidden');
				$('.whp-modal-slider').removeClass('show');
				$('.whp-modal-list-header').removeClass('show');
				setTimeout(function() {
					$('.whp-modal-slider').addClass('hidden');
					$('.whp-modal-list').removeClass('hidden');
					setTimeout(function() {
						$('.whp-modal-list').addClass('show');
					},200);
				},300);
			});

			var whnoModalHoldOpen=false;
			$('.whno-modal-slider').on('translated.owl.carousel', function(event) {
				if(!whnoModalHoldOpen){
					whnoModalHoldOpen=true;
					var $sliderItem = $('.whno-modal-slider .owl-item.active .wh-slider-item');
					var name = $sliderItem.data('title');
					var level = $sliderItem.data('level');
					var desc = $sliderItem.data('desc');

					var $modalHeader = $('.whno-modal-header');
					$modalHeader.children('.whno-title').text(name);
					$modalHeader.children('.whno-level').text(level);
					$modalHeader.children('.whno-desc').text(desc);
				}
				whnoModalHoldOpen=false;
			});
		}
	});
	$('.close-whats-happening').click(function(){
		$('.whats-happening-item').removeClass('active');
		$('.whats-happening-item').removeClass('not-active');
	});
	$('.wh-slider').owlCarousel({
		items:1,
		loop:false,
		nav:true,
		navText: ['<div class="banner-nav prev"><span class="fa fa-angle-left"></span></div>','<div class="banner-nav next"><span class="fa fa-angle-right"></span></div>']
	});
	$('.zoom-button').on('click',function() {
		var $this = $(this);
		var $container = $this.parent().parent().children('.zoom-container');
		var $image = $container.children('.zoom-image');
		var $close = $container.parent().parent().children('.zoom-close');

		$image.attr('src',$this.attr('data-image'));
		$container.addClass('active');
		$close.addClass('active');
	});
	$('.zoom-close').on('click',function() {
		var $this = $(this);
		var $container = $this.parent().find('.zoom-container');
		var $image = $container.children('.zoom-image');
		$container.removeClass('active');
		$this.removeClass('active');
		$image.attr('src','');
	});
	$('.wh-modals .wh-slider').on('translated.owl.carousel', function(event){
		var $this = $(this);
		var $sliderItem = $this.find('.owl-item.active .wh-slider-item');
		var $slideritemImg = $sliderItem.children('img');
		$this.parent().children('.zoom-button').attr('data-image',$slideritemImg.attr('src'));
	});
	$('.whp-item').on('click',function() {
		var $this = $(this);
		var slider = $this.data('slider');

		$('.whp-item').removeClass('active');
		$this.addClass('active');
		$('.whp-slider.active').fadeOut(300,function() {
			$('.whp-slider.active').removeClass('active');
			$('.' + slider).fadeIn(300,function() {
				$('.' + slider).addClass('active');
			});
		});
	});
	var whnoHoldOpen=false;
	$('.whno-slider').on('changed.owl.carousel', function(event){
		if(!whnoHoldOpen){
			var $sidebar = $('.whats-happening-sidebar.nowopen .whats-happening-sidebar-content');
			$sidebar.children('.whno-title').fadeOut(100);
			$sidebar.children('.whno-level').fadeOut(100);
			$sidebar.children('.whno-desc').fadeOut(100);
		}
	});
	$('.whno-slider').on('translated.owl.carousel', function(event) {
		if(!whnoHoldOpen){
			whnoHoldOpen=true;
			var $sliderItem = $('.whno-slider .owl-item.active .wh-slider-item');
			var name = $sliderItem.data('title');
			var level = $sliderItem.data('level');
			var desc = $sliderItem.data('desc');

			var $sidebar = $('.whats-happening-sidebar.nowopen .whats-happening-sidebar-content');
			$sidebar.children('.whno-title').text(name);
			$sidebar.children('.whno-level').text(level);
			$sidebar.children('.whno-desc').text(desc);
			$sidebar.children('.whno-title').fadeIn();
			$sidebar.children('.whno-level').fadeIn();
			$sidebar.children('.whno-desc').fadeIn();
		}
		whnoHoldOpen=false;
	});

	/** end of What's Happening Section **/

	/** Instagram Feed Section **/
	var feed = new Instafeed({
        get: 'user',
        userId: '336158374',
        accessToken: '336158374.1677ed0.62fd1ccce89f4d8cbc47572a4a6af386',
        limit: 6,
        resolution: 'standard_resolution',
        template: '<div class="col-sm-4 instagram-item {{orientation}}"><a target="_blank" href="{{link}}"><div style="background-image:url({{image}});background-size:cover;background-position:center;background-repeat:no-repeat"><img class="img-responsive" src="./res/images/ig-placeholder.jpg" style="opacity:0"/></div><div class="instagram-caption-container"><div class="instagram-caption-content"><div class="instagram-caption">{{caption}}</div><div class="view-on-instagram">view on instagram</div></div></div></a></div>'
    });
    feed.run();
	/** end of Instagram Section **/
    //$("#instagram-feed").slick({
    //    infinite: true,
    //    slidesToShow: 3,
    //    slidesToScroll: 3
    //});
	/** Highlights Section **/
	if($('.highlights-slider .item').length > 1) {
		$('.highlights-slider').owlCarousel({
			items:1,
				loop:true,
				nav:true,
				navText: ['<div class="banner-nav prev"><span class="fa fa-angle-left"></span></div>','<div class="banner-nav next"><span class="fa fa-angle-right"></span></div>']
		});
	} else {
		$('.highlights-slider').owlCarousel({
			items:1,
				loop:false,
				nav:true,
				navText: ['<div class="banner-nav prev"><span class="fa fa-angle-left"></span></div>','<div class="banner-nav next"><span class="fa fa-angle-right"></span></div>']
		});
	}
	/** end of Highlights Section **/

	/** Blog Section **/
	$('.blog-slider').owlCarousel({
		items:blogItem,
		loop:true,
		margin:10,
		dots:true,
		dotsEach:true
	});
	$('.blog-details-slider').owlCarousel({
		items:blogItem,
		margin:10,
		nav:true,
		navText: ['<div class="banner-nav prev"><span class="fa fa-angle-left"></span></div>','<div class="banner-nav next"><span class="fa fa-angle-right"></span></div>']
	});
	$('.blog-slider .see-more').click(function(){
		var title = $(this).parent().children('.title').text();
		var description = $(this).parent().children('.content-full').html();
		var data = $(this).data('blogimages');
		var images = data.split('>');
		var htmldata = '';
		var i = 0;
		$('.blog-details-slider').trigger('destroy.owl.carousel');
		$('.blog-details-slider').removeClass('owl-carousel owl-loaded');
		$('.blog-details-slider').find('.owl-stage-outer').children().unwrap();
		images.forEach(function(value){
			if(i!=0){
				htmldata+= '<div class="item" style="background-image:url('+ value +');background-size:100% 100%;"><img src="'+value+'" alt="Plaza Indonesia Blog" style="opacity:0;"></div>';
			}
			i++;
		});
		$('.blog-details-slider').html(htmldata);
		$('.blog-details-wrapper').addClass('active');
		$('.blog-details-slider').addClass('owl-carousel');
		$('.blog-details-slider').owlCarousel({
			items:blogItem,
			margin:10,
			nav:true,
			navText: ['<div class="banner-nav prev"><span class="fa fa-angle-left"></span></div>','<div class="banner-nav next"><span class="fa fa-angle-right"></span></div>']
		});
		$('.blog-details .title').text(title);
		$('.blog-details .content').html(description);
	});
	$('.close-blog').click(function(){
		$('.blog-details-wrapper').removeClass('active');
	});
	/** end of Blog Section **/

	/** Shopping Privilege Section **/
	var privilegeHold = false;
	var privilegeHoldTimer;
	$('.privilege-item').click(function(){
		if(!privilegeHold){
			privilegeHold = true;
			$('.privilege-item').not(this).addClass('not-active');
			$(this).addClass('active');
			clearTimeout(privilegeHoldTimer);
			privilegeHoldTimer = setTimeout(function(){
				privilegeHold = false;
			}, 1000);
		}
	});
	$('.close-privilege').click(function(){
		$('.privilege-item').removeClass('active');
		$('.privilege-item').removeClass('not-active');
	});
	$('#touristDate').datepicker();
	$('.terms-language-option').on('click',function() {
		var $this = $(this);
		var lang = $this.data('lang');
		$('.terms-language-option').removeClass('active');
		$this.addClass('active');
		
		$('.privilege-modal-body').removeClass('active');
		$('.privilege-modal-body.' + lang).addClass('active');
	});
	function ajaxTourist(url){
		$.ajax({
			type: "POST",
			url: url,
			data: $( "#tourist-pass-register" ).serialize(),
			dataType: "text",
			timeout:30000
		}).done(function( data ) {
			$('#tourist-error-msg').text(data);
			if(!data){
				$('#touristPassModal').modal('hide');
				$('#promoModal').modal('show');
				$('#tourist-pass-register').trigger('reset');
				grecaptcha.reset();
			}
		}).fail(function(jqxhr, textStatus, error) {
			console.log(textStatus);
			console.log(error);
		});
		return false;
	}
	function ajaxRegister(url){
		var formData = $( "#privilege-card-form" ).serializeArray();
		var sendData = {
			cardMember : formData[0].value,
			mobilePhone : formData[1].value
		};
		$.ajax({
			type: "POST",
			//url: "http://10.23.2.58:8011/PIWSWeb.asmx/LoginMember",
			//url: "http://10.196.1.58:8011/PIWSWeb.asmx/LoginMember",
			url: "<?php echo site_url('privilege/login_dummy'); ?>",
			//data: JSON.stringify(sendData), //JANGAN LUPA DIBALIKIN
			data: sendData,
			//contentType: 'application/json', //JANGAN LUPA DIBALIKIN
			dataType: "json",
			timeout:30000
		}).done(function( data ) {
			if(data.d == null){
				alert('Invalid Card Number or Phone Number.');
			}else{
				$.post("<?php echo site_url('privilege/set_data'); ?>",
					{
						data: data.d
					},
					function(data,status){
					var popup = window.open("<?php echo site_url('privilege'); ?>", '_blank');
					setTimeout( function() {
						if(!popup || popup.outerHeight === 0) {
							//First Checking Condition Works For IE & Firefox
							//Second Checking Condition Works For Chrome
							alert("Popup Blocker is enabled! Please add this site to your exception list.");
						} else {
						} 
					}, 500);
					});
			}
		}).fail(function(jqxhr, textStatus, error) {
			console.log(jqxhr);
			console.log(textStatus);
			console.log(error);
			alert(textStatus);
		});
        return false;
    }
    $('.pagination-item').on('click', function () {
        var $this = $(this);
        if (!$this.hasClass('active') && !$this.hasClass('pagination-nav')) {
            var $current = $('.pagination-item.active');
            var curPage = $current.data('page');
            $current.removeClass('active');
            $('.reward-page[data-page="' + curPage + '"]').addClass('hidden');
            var page = $this.data('page');
            $this.addClass('active');
            $('.reward-page[data-page="' + page + '"]').removeClass('hidden');
        }
    });
    $('.pagination-nav').on('click', function () {
        var $this = $(this);
        var $current = $('.pagination-item.active');
        var curPage = $current.data('page');

        var nextPage = curPage;
        if ($this.hasClass('pagination-prev')) nextPage--;
        else if ($this.hasClass('pagination-next')) nextPage++;
        var $next = $('.pagination-item[data-page="' + nextPage + '"]');
        if ($next.length !== 0) {
            $current.removeClass('active');
            $('.reward-page[data-page="' + curPage + '"]').addClass('hidden');
            $next.addClass('active');
            $('.reward-page[data-page="' + nextPage + '"]').removeClass('hidden');
        }
    });
	/** end of Shopping Privilege Section **/

	/** Directory Section **/
	var storesHold = 0;
	/*
		Contoh Format json directory:

		"id_store":"769",
		"id_ref_store_category":"32",
		"id_ref_publish":"1",
		"kav":"0",
		"store":"AMKC Atelier",
		"description":"",
		"phone":"(021) 2992-4357/ 4356",
		"fax":"",
		"email":"",
		"image_logo":"",
		"image_1":null,
		"image_2":null,
		"image_promo":null,
		"image_peta":"1456304373.jpg",
		"kode":"",
		"lantai":"1",
		"promo_link":"./promo.html",
		"posisi_x":null,
		"posisi_y":null
	*/
	var storesSample = [
		{"id_store":"769","id_ref_store_category":"32","id_ref_publish":"1","kav":"0","store":"AMKC Atelier","description":"","phone":"(021) 2992-4357/ 4356","fax":"","email":"","image_logo":"","image_1":null,"image_2":null,"image_promo":null,"image_peta":"1456304373.jpg","kode":"","lantai":"1","promo_link":"./promo.html","posisi_x":null,"posisi_y":null},
		{"id_store":"397","id_ref_store_category":"32","id_ref_publish":"1","kav":"3","store":"Bistro Baron","description":"","phone":"(021) 2992 3505","fax":"(021) 299 23506","email":"","image_logo":"","image_1":null,"image_2":null,"image_promo":null,"image_peta":"1472634916.jpg","kode":"","lantai":"1","promo_link":"./promo.html","posisi_x":"0","posisi_y":"0"},
		{"id_store":"522","id_ref_store_category":"32","id_ref_publish":"1","kav":"0","store":"Chandara Fine Thai Cuisine","description":"","phone":"(021) 2992 3775","fax":"","email":"","image_logo":"1425552366-logo.jpg","image_1":null,"image_2":null,"image_promo":null,"image_peta":"201302140931500.chandara.jpg","kode":"","lantai":"1","promo_link":"./promo.html","posisi_x":"0","posisi_y":"0"},
		{"id_store":"4","id_ref_store_category":"32","id_ref_publish":"1","kav":"61","store":"Cork &amp; Screw","description":"   <br />\n","phone":"(021) 319 96659","fax":"","email":"","image_logo":"201002261526040.COrk-n-Screw.jpg","image_1":"200910061207120.Cork-&-Screw-label.png","image_2":"200910061207120.Cork-&-Screw.png","image_promo":null,"image_peta":"1415167430.jpg","kode":"","lantai":"1","promo_link":"./promo.html","posisi_x":"310","posisi_y":"530"},
		{"id_store":"304","id_ref_store_category":"32","id_ref_publish":"1","kav":"16","store":"Gyukaku","description":"","phone":"(021) 310 7196","fax":"","email":"","image_logo":"1421137592-logo.jpg","image_1":"200910261049050.Gyukaku-label.png","image_2":"200910261049050.Gyukaku.png","image_promo":null,"image_peta":"201008310305290.icon.jpg","kode":"","lantai":"1","promo_link":"./promo.html","posisi_x":"365","posisi_y":"505"},
		{"id_store":"307","id_ref_store_category":"32","id_ref_publish":"1","kav":"60","store":"Imperial Shangai La Mian Xiao Long Bao","description":"","phone":"(021) 398 35100","fax":"(021) 3983 5101","email":"","image_logo":"","image_1":"200910261051440.Imperial-Treasure-La-Mian-Xiao-Long-Bao-label.png","image_2":"200910261051440.Imperial-Treasure-La-Mian-Xiao-Long-Bao.png","image_promo":null,"image_peta":"201101031852030.Iperial-Treasure-icon.jpg","kode":"","lantai":"1","promo_link":"./promo.html","posisi_x":"315","posisi_y":"505"},
		{"id_store":"700","id_ref_store_category":"32","id_ref_publish":"1","kav":"0","store":"La Moda","description":"","phone":"(021) 2992 4217","fax":"","email":"","image_logo":"1429260830-logo.jpg","image_1":null,"image_2":null,"image_promo":null,"image_peta":"1429260950.jpg","kode":"","lantai":"1","promo_link":"./promo.html","posisi_x":null,"posisi_y":null},
		{"id_store":"326","id_ref_store_category":"32","id_ref_publish":"1","kav":"51","store":"Segafredo Zanetti","description":"","phone":"(021) 315 0279","fax":"","email":"","image_logo":"201002241139410.Segafredo.jpg","image_1":"200910261127430.Segafredo-Zanetti-label.png","image_2":"200910261127430.Segafredo-Zanetti.png","image_promo":null,"image_peta":"201008311210270.icon.jpg","kode":"","lantai":"1","promo_link":"./promo.html","posisi_x":"205","posisi_y":"470"},
		{"id_store":"461","id_ref_store_category":"32","id_ref_publish":"1","kav":"92","store":"Shabu Shabu House","description":"","phone":"(021) 2992 3676","fax":"","email":"","image_logo":"1421123297-logo.jpg","image_1":null,"image_2":null,"image_promo":null,"image_peta":"201108181132100.icon.jpg","kode":"","lantai":"1","promo_link":"./promo.html","posisi_x":"0","posisi_y":"0"},
		{"id_store":"739","id_ref_store_category":"32","id_ref_publish":"1","kav":"0","store":"SociEATy by the Les Amis Group","description":"","phone":"(021) 2992 3888","fax":"","email":"","image_logo":"","image_1":null,"image_2":null,"image_promo":null,"image_peta":"1443152561.jpg","kode":"","lantai":"1","promo_link":"./promo.html","posisi_x":null,"posisi_y":null},
		{"id_store":"329","id_ref_store_category":"32","id_ref_publish":"1","kav":"64","store":"Starbucks Coffee","description":"","phone":"(021) 398 38805","fax":"","email":"","image_logo":"1421137818-logo.jpg","image_1":"200910261131380.Starbucks-Coffee-label.png","image_2":"200910261131380.Starbucks-Coffee.png","image_promo":null,"image_peta":"201203211741280.starbuck-icon.jpg","kode":"","lantai":"1","promo_link":"./promo.html","posisi_x":"180","posisi_y":"450"},
		{"id_store":"17","id_ref_store_category":"32","id_ref_publish":"1","kav":"12","store":"Sushi Tei","description":"","phone":"(021) 398 35108 / 09","fax":"","email":"","image_logo":"1421136752-logo.jpg","image_1":"200910061308510.Sushi-Tei-label.png","image_2":"200910061308510.Sushi-Tei.png","image_promo":null,"image_peta":"map-sushitei.jpg","kode":"","lantai":"1","promo_link":"","posisi_x":"380","posisi_y":"450"},
		{"id_store":"332","id_ref_store_category":"32","id_ref_publish":"1","kav":"14","store":"The Coffee Bean &amp; Tea Leaf","description":"","phone":"(021) 398 38778","fax":"","email":"","image_logo":"1421137912-logo.jpg","image_1":"200910261135540.The-Coffee-Bean-&-Tea-Leaf-label.png","image_2":"200910261135540.The-Coffee-Bean-&-Tea-Leaf.png","image_promo":null,"image_peta":"1478680137.jpg","kode":"","lantai":"1","promo_link":"","posisi_x":"348","posisi_y":"455"}
	];
	$('.search-categories').perfectScrollbar({
		suppressScrollX : true
	});
	$('.categories').perfectScrollbar({
		suppressScrollX : true
	});
	$('.shops').perfectScrollbar({
		suppressScrollX : true
	});
	$('.selected-category-wrapper').click(function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
		}else{
			$(this).addClass('active');
		}
	});
	$('.search-categories .item').click(function(){
		var category = $(this).text();
		var catid = $(this).data('id');
		$('#store-category').val(catid);
		$('.selected-category').text(category);
		$('.selected-category-wrapper').removeClass('active');
	});
	$('.search-top').keyup(function(e){
	    if(e.keyCode == 13) {
			$('#store-keyword').val($('.search-top').val());
			$('#directory-menu').trigger('click');
			$('.mobile-menu').removeClass('active');
	    	$('.search-top').blur();
			var search_top = setTimeout(function() {
	    		$('.search-top').val('');
	    		$("#search-store").submit();
			},800);
	    }
	});
	$( "#search-store").submit(function( event ) {
		if(storesHold == 0){
			storesHold = 1;
			var category = $('#store-category').val();
			var keyword = $('#store-keyword').val();
			event.preventDefault();
			$('.level-wrapper').addClass('active');
			$('.categories').html('<div>Loading...</div>');
			$('.shops').html('');
			$('.level-text').removeClass('active');
			$('.shop-details').removeClass('active');
			$('#levelSearch').addClass('active');
			/**
				Fungsi setTimeout hanya untuk sebagai pengganti fungsi AJAX.
				Detail fungsi ajax ada pada komen berikutnya.
			**/
		
					$.ajax({
						type: "POST",
                        url: "/index/SearchStore",
						data:  { category: category, keyword: keyword },		
						dataType: "json",
						success: function(data) {
                            var htmldata = '';
                            if (data == '') {
                                htmldata = '<div>No Result Found.</div>';
                            } else {
                                data.forEach(function (value) {
                                    if (value['lantai'] == 'LB') {
                                        value['lantai'] = 'Basement';
                                    }
                                    htmldata += '<div class="shop">' + value['store']
                                    htmldata += '<div class="shop-data" data-store="' + value['store'] + '" data-level="' + value['lantai'] + '" data-code="' + value['kode'] + '"';
                                    if (value['image_peta']) {
                                        htmldata += ' data-image="' + value['image_peta'] + '"';
                                    } else {
                                        htmldata += ' data-image="store-na-img.jpg"';
                                    }
                                    if (value['phone']) {
                                        htmldata += ' data-phone="' + value['phone'] + '"';
                                    } else {
                                        htmldata += ' data-phone="Not Available"';
                                    }
                                    htmldata += '></div></div>';
                                });
                            }
                            $('.categories').html(htmldata);
						},
						fail: function( jqXHR, textStatus ) {
							alert( "Request failed: " + textStatus );
						}
					});
				
				var htmldata = '';
				//if(storesSample == '') {
				//	htmldata = '<div>No Result Found.</div>';
				//} else {
				//	storesSample.forEach(function(value){
				//		if(value['lantai']=='LB'){
				//			value['lantai'] = 'Basement';
				//		}
				//		htmldata += '<div class="shop">'+value['store']+' '+value['kode'];
				//		htmldata += '<div class="shop-data" data-store="'+value['store']+'" data-level="'+value['lantai']+'" data-code="'+value['kode']+'" data-promo-link="'+value['promo_link']+'"';
				//		if(value['image_peta']) {
				//			htmldata += ' data-image="'+value['image_peta']+'"';
				//		}else{
				//			htmldata += ' data-image="store-na-img.jpg"';
				//		}
				//		if(value['phone']){
				//			htmldata += ' data-phone="'+value['phone']+'"';
				//		}else{
				//			htmldata += ' data-phone="Not Available"';
				//		}
				//		htmldata += '></div></div>';
				//	});
				//}
				$('.categories').html(htmldata);
				storesHold = 0;

		}
	});
	$('.level-text').click(function(){
		var data = $(this).children('.level-data').data('category');
		var level = $(this).children('.level-data').data('level');
		var categories = data.split('>');
		var htmldata = '';
		var i = 0;
		categories.forEach(function(value){
			if(i!=0){
				var category_split = value.split('}');
				htmldata += '<div class="category" data-level="'+level+'" data-catid="'+category_split[0]+'">'+category_split[1]+'</div>';
			}
			i++;
		});
		$('.shops').html('');
		$('.categories').html(htmldata);
		$('.level-wrapper').addClass('active');
		$('.level-text').removeClass('active');
		$(this).addClass('active');
	});
	$('.categories').on('click', '.category', function(){
		if(storesHold == 0){
			storesHold = 1;
			var level = $(this).data('level');
			var cat_id = $(this).data('catid');
			$('.shops').html('<div style="margin-left:15px;">Loading...</div>');
			/**
				Fungsi setTimeout hanya untuk sebagai pengganti fungsi AJAX.
				Detail fungsi ajax ada pada komen berikutnya.
			**/


					$.ajax({
						type: "POST",
                        url: "/Index/GetStoreLevel",
						data:  { cat_id: cat_id, level: level },		
						dataType: "json",
                        success: function (data) {
                            var htmldata = '';
                            data.forEach(function (value) {
                                htmldata += '<div class="shop">' + value['store']
                                htmldata += '<div class="shop-data" data-store="' + value['store'] + '" data-level="' + value['lantai'] + '" data-code="' + value['kode'] + '"';
                                if (value['image_peta']) {
                                    htmldata += ' data-image="' + value['image_peta'] + '"';
                                } else {
                                    htmldata += ' data-image="store-na-img.jpg"';
                                }
                                if (value['phone']) {
                                    htmldata += ' data-phone="' + value['phone'] + '"';
                                } else {
                                    htmldata += ' data-phone="Not Available"';
                                }
                                htmldata += '></div></div>';
                            });
                            $('.shops').html(htmldata);
                            storesHold = 0;
                        
						},
						fail: function( jqXHR, textStatus ) {
							alert( "Request failed: " + textStatus );
							storesHold=0;
						}
					});
				//var htmldata = '';
				//storesSample.forEach(function(value){
				//	htmldata += '<div class="shop">'+value['store']+' '+value['kode'];
				//	htmldata += '<div class="shop-data" data-store="'+value['store']+'" data-level="'+value['lantai']+'" data-code="'+value['kode']+'" data-promo-link="'+value['promo_link']+'"';
				//	if(value['image_peta']) {
				//		htmldata += ' data-image="'+value['image_peta']+'"';
				//	}else{
				//		htmldata += ' data-image="store-na-img.jpg"';
				//	}
				//	if(value['phone']){
				//		htmldata += ' data-phone="'+value['phone']+'"';
				//	}else{
				//		htmldata += ' data-phone="Not Available"';
				//	}
				//	htmldata += '></div></div>';
				//});
				//$('.shops').html(htmldata);
				//storesHold=0;
		}
	});
	$('.directory-close').click(function() {
		$('.level-wrapper').removeClass('active');
		$('.level-text').removeClass('active');
	});
	$('.categories').on('click', '.shop', function(){
		var name = $(this).children('.shop-data').data('store');
		var level = "Level " + $(this).children('.shop-data').data('level');
		var code = $(this).children('.shop-data').data('code');
		var phone = $(this).children('.shop-data').data('phone');
		var promo_link = $(this).children('.shop-data').data('promo-link');
		var image = './res/images/directory/'+$(this).children('.shop-data').data('image');
		$('.shop-details .image-wrapper>img').attr('src', image);
		$('.shop-details .phone').text(phone);
		$('.shop-details .title').text(name);
		$('.shop-details .level').text(level);
		$('.shop-details .code').text(code);
		$('.shop-details .promo-link').attr('href',promo_link);
		if(promo_link == '') {
			$('.shop-details .promo-link').addClass('hidden');
		} else {
			$('.shop-details .promo-link.hidden').removeClass('hidden');
		}
		$('.shop-details').addClass('active');
	});
	$('.shops').on('click', '.shop', function(){
		var name = $(this).children('.shop-data').data('store');
		var level = 'Level '+$(this).children('.shop-data').data('level');
		var code = $(this).children('.shop-data').data('code');
		var phone = $(this).children('.shop-data').data('phone');
		var promo_link = $(this).children('.shop-data').data('promo-link');
		var image = './res/images/directory/'+$(this).children('.shop-data').data('image');
		$('.shop-details .image-wrapper>img').attr('src', image);
		$('.shop-details .phone').text(phone);
		$('.shop-details .title').text(name);
		$('.shop-details .level').text(level);
		$('.shop-details .code').text(code);
		$('.shop-details .promo-link').attr('href',promo_link);
		if(promo_link == '') {
			$('.shop-details .promo-link').addClass('hidden');
		} else {
			$('.shop-details .promo-link.hidden').removeClass('hidden');
		}
		$('.shop-details').addClass('active');
	});
	$('.shop-close').click(function(){
		$('.shop-details').removeClass('active');
	});
	/** end of Directory Section **/

	/** Contact Section **/
	var ajax_contact_process = false;
	var ajax_contact_timer;
	var ajax_contact_interval;
	var ajax_contact_error = 0;
	var ajax_contact_retry = 5;
	function ajax_contact(url) {
		clearInterval(ajax_contact_interval);
		clearTimeout(ajax_contact_timer);
		$('.ajax-notification-container').addClass('show');
		$('.ajax-notification-content').html('Sending...');
		if(!ajax_contact_process) {
			ajax_contact_process = true;
			ajax_contact_timer = setTimeout(function() {
				$.ajax({
				   type: "POST",
				   url: url,
				   data: $( "#contact_form" ).serialize(),
				   dataType: "json",
				   timeout:30000
				}).done(function( data ) {
					ajax_contact_process = false;
					ajax_contact_error = 0;
					ajax_contact_retry = 5;
					$('.ajax-notification-content').html(data.message);
					grecaptcha.reset();
					if(data.success == 1) {
						$('#contact_form')[0].reset();
						ajax_contact_timer = setTimeout(function() {
							$('.ajax-notification-container').removeClass('show');
						},10000);
					}
				}).fail(function(jqxhr, textStatus, error) {
					ajax_contact_process = false;
					ajax_contact_error++;
					
					var time_to_retry = ajax_contact_retry * ajax_contact_error;
					var i = time_to_retry;
					$('.ajax-notification-content').html('Sending failed... Retrying in ' + i + '...');
					ajax_contact_interval = setInterval(function() {
						$('.ajax-notification-content').html('Sending failed... Retrying in ' + i + '...');
						i--;
					},1000);
					ajax_contact_timer = setTimeout(function() {
						ajax_contact(url);
					},time_to_retry * 1000);
				   console.error("AJAX failed, status: " + textStatus + ", error: "+error);
				});
			},1000);
		}
		
		return false;
    }
    var banner = getUrlParameter('banner');
    if (banner  != undefined) {
        $("#home-slider-modal-" + banner).modal();
    }
     function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    
	/** end of Contact Section **/

	
});

/** Animation Scripts **/
$(window).bind("load", function() {
	$('.animate').waypoint(function(direction) {
		this.element.classList.add("animated");
	}, {
		offset: '75%'
	});
});
/** end of Animation Scripts **/

/** Google Map **/
window.onload = function () { "use strict"; var e = new google.maps.LatLng(-6.193327, 106.821972), a = [], o = { zoom: 16, center: e, mapTypeId: google.maps.MapTypeId.ROADMAP, disableDefaultUI: !0, scrollwheel: !1, styles: a }, n = '<div id="content"><div id="siteNotice"></div><h4>We Are Here!</h4><p>Jalan M.H. Thamrin No.28-30</p><p>Jakarta Pusat</p><p>DKI Jakarta 10350, Indonesia</p></div>', t = new google.maps.InfoWindow({ content: n }), i = new google.maps.Map(document.getElementById("map"), o), p = new google.maps.LatLng(-6.193327, 106.821972), s = "./res/images/map-marker.png", g = new google.maps.Marker({ position: p, map: i, title: "Plaza Indonesia", icon: s, url: 'https://www.google.co.id/maps/place/Plaza+Indonesia/@-6.1930596,106.8197599,17z/data=!3m2!4b1!5s0x2e69f4211614e4ed:0x7aefc3305250ccc5!4m5!3m4!1s0x2e69f421742da401:0xeaecc0caede16dd0!8m2!3d-6.1930596!4d106.8219486?hl=en' }); google.maps.event.addListener(g, "click", function () { window.open(this.url, '_blank') }) };