$(document).ready(function () {

	/** Mobile Responsive Settings **/
	var winW = $(window).width();
	var device = 0;
	if(winW > 992 && winW  < 1200) {
		device = 1;
	} else if(winW < 992) {
		device = 2;
	}
	var blogItem = 4;
	var nowOpen = 0;
	var eventItem = 3;
	if(device == 0){
		nowOpen = 225;
	} else if(device == 1) {
		nowOpen = 100;
	} else if(device == 2) {
		blogItem = 2;
		eventItem = 2;
	}
	/** end of Mobile Responsive Settings **/

	/** Init **/
	$(".dot").dotdotdot({
		watch: window
	});
	/** end of Init **/

	/** Menu **/
	$('#menu').affix({
		offset: {
			top: function () {
				return $('#home').outerHeight(true);
			}
		}
	});
	$(window).scroll(function (event) {
		var scroll = $(window).scrollTop();
		var iheight = $('#home').innerHeight();
		var sheight = this.scrollHeight;
		if(scroll>100){
			$('.sub-menu').addClass('bot');
		}else{
			$('.sub-menu').removeClass('bot');
		}
	});
	$('.menu-bar').click(function(){
		var barhas = $('.mobile-menu').hasClass('active');
		if(barhas){
			$('.mobile-menu').removeClass('active');
		}else{
			$('.mobile-menu').addClass('active');
		}
	});
	$('.mobile-menu li:not(.wo):not(.search)').click(function(){
		$('.mobile-menu').removeClass('active');
	});
	$('.wo').click(function(){
		if($('.wo .sub-menu').hasClass('active')){
			$('.wo .sub-menu').removeClass('active');
		}else{
			$('.wo .sub-menu').addClass('active');
		}
	});
	$('#menu li a').click(function(){
		var value = $(this).attr('href');
		$('html, body').animate({
			'scrollTop': $(value).offset().top-50
		});
	});
	$('.mobile-menu li a').click(function(){
		var value = $(this).attr('href');
		$('html, body').animate({
			'scrollTop': $(value).offset().top-50
		});
	});
	/** end of Menu **/

	/** Banner Section **/
	$('.event-slider').owlCarousel({
		items:1,
		loop:false
		// autoplay:true
	});
	/** end of Banner Section **/

	/** Timeline Section **/
    function getDataPIFF(year) {
        $.ajax({
            type: 'POST',
            url: 'PIFF/GetPIFFAjaxData',
            data: JSON.stringify({ year: year }),
            dataType: 'JSON',
            contentType: 'application/json;',
            success: function (data) {
                var container = $("#movieContent")
                //remove prev element
                container.children().remove();
                //append new loaded element
                $.each(data, function (k, v) {
                    var image = v.Image;
                    var active = "active";
                    if (k > 0) {
                        active = "";
                    }
                    container.append(" <div class='movie-item item " + active + "'> <div class='movie-image' style='background-image:url(" + image + ");'><img class='img-responsive' src='~/res/images/event-nowshowing-placeholder.jpg'></div><div class='title'>" + v.EventName + "</div><div class='created'>Directed by Brillante Mendoza, 2016</div><div class='synopsis-button' data-image='" + image + "' data-title='" + v.EventName + "' data-created='Directed by Brillante Mendoza' data-synopsis='" + v.Description + "'>Synopsis</div><div class='pick-group'><div class='movie-date'>" + v.StartDate + "</div></div>")
                });
                //add click event on new appended element
                $(document).on('click', '.synopsis-button', function (e) {

                    var image = $(this).data('image');
                    var title = $(this).data('title');
                    var created = $(this).data('created');
                    var synopsis = $(this).data('synopsis');
                    $('.synopsis-image img').attr('src', image);
                    $('.synopsis-title').html(title);
                    $('.synopsis-created').html(created);
                    $('.synopsis-text').html(synopsis);
                    $('#event-nowshowing-synopsis-wrapper').fadeIn();

                });

                var currentTime = new Date();
                if (year != currentTime.getFullYear()) {
                    $("#event-form").css("display", "none");
                }
                else {
                    $("#event-form").css("display", "block");
                }
            }
        })
    };
    $(".timeline-item[data-index=5]").addClass('active')
	$('.timeline-item:not(active)').on('click',function() {
		$('.timeline-item').removeClass('active');
        $(this).addClass('active');
        var yearVal = $(this)[0].innerText;
        getDataPIFF(yearVal);
	});
	$('.timeline-prev').on('click',function() {
		var curIndex = $('.timeline-item.active').data('index');
		if(curIndex > 1) {
			curIndex--;
			$('.timeline-item').removeClass('active');
			$('.timeline-item[data-index=' + curIndex + ']').addClass('active');
		}
	});
	$('.timeline-next').on('click',function() {
		var maxIndex = $('.timeline-list').data('max');
		var curIndex = $('.timeline-item.active').data('index');
		if(curIndex < maxIndex) {
			curIndex++;
			$('.timeline-item').removeClass('active');
			$('.timeline-item[data-index=' + curIndex + ']').addClass('active');
		}
	});
	/** end of Timeline Section **/

	/** Form Section **/
	$('.event-modal-synopsis-button').on('click',function() {
		var image = $(this).data('image');
		var title = $(this).data('title');
		var created = $(this).data('created');
		var synopsis = $(this).data('synopsis');
		$('.event-modal-synopsis-image img').attr('src', image);
		$('.event-modal-synopsis-title').html(title);
		$('.event-modal-synopsis-created').html(created);
		$('.event-modal-synopsis-text').html(synopsis);
		$('#event-modal-synopsis-wrapper').fadeIn();
	});
	$('.event-modal-close-synopsis').click(function(){
		$('#event-modal-synopsis-wrapper').fadeOut();
	});
	/** end of Form Section **/

	/** Now Showing Section **/
    $('.synopsis-button').on('click','.movie-container',function() {
		var image = $(this).data('image');
		var title = $(this).data('title');
		var created = $(this).data('created');
		var synopsis = $(this).data('synopsis');
		$('.synopsis-image img').attr('src', image);
		$('.synopsis-title').html(title);
		$('.synopsis-created').html(created);
		$('.synopsis-text').html(synopsis);
		$('#event-nowshowing-synopsis-wrapper').fadeIn();
	});
	$('.close-synopsis').click(function(){
		$('#event-nowshowing-synopsis-wrapper').fadeOut();
    });
    
	/** end of Now Showing Section **/
});