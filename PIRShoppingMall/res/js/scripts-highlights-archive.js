$(document).ready(function() {
	/** Mobile Responsive Settings **/
	var winW = $(window).width();
	var device = 0;
	if(winW > 992 && winW  < 1200) {
		device = 1;
	} else if(winW < 992) {
		device = 2;
	}
	var blogItem = 4;
	var nowOpen = 0;
	var eventItem = 3;
	if(device == 0){
		nowOpen = 225;
	} else if(device == 1) {
		nowOpen = 100;
	} else if(device == 2) {
		blogItem = 2;
		eventItem = 2;
	}
	/** end of Mobile Responsive Settings **/

	/** Init **/
	$(".dot").dotdotdot({
		watch: window
	});
	/** end of Init **/

	/** Menu **/
	
	/** end of Menu **/

	/** Banner Section **/
	$('.highlights-slider').owlCarousel({
		items:1,
		loop:true,
		nav:true,
		navText: ['<div class="banner-nav prev"><span class="fa fa-angle-left"></span></div>','<div class="banner-nav next"><span class="fa fa-angle-right"></span></div>']
	});
	/** end of Banner Section **/
});