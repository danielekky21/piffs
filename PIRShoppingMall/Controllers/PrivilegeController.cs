﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.model;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Text;
using System.Web.Http.Cors;

namespace PIRShoppingMall.Controllers
{
    [EnableCors(origins: "http://10.196.1.58:8011",headers: "*",methods: "*")]
    public partial class PrivilegeController : Controller
    {
        // GET: Privilege
        public virtual ActionResult Login()
        {
            return View();
        }
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult PrivilegeHome(CardMember model)
        {
            cardMemberResponse obj = new cardMemberResponse();
            string  postdata = "cardMember=" + model.cardMember + "&mobilePhone=" + model.mobilePhone;
            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] byteArray = Encoding.UTF8.GetBytes(postdata);
            string url = "http://117.102.84.181:8011/PIWSWeb.asmx/LoginMember?cardMember=" + model.cardMember + "&mobilePhone=" + model.mobilePhone;
            var serializer = new JsonSerializer();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://117.102.84.181:8011/PIWSWeb.asmx/LoginMember");
            httpWebRequest.ContentType = "application/json";
            
            httpWebRequest.Method = "POST";
            //httpWebRequest.ContentLength = byteArray.Length;
            //Stream dataStream = httpWebRequest.GetRequestStream();

            //dataStream.Write(byteArray, 0, byteArray.Length);
            //dataStream.Close();
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(model);
                //string json = "{\"cardMember\":\""+ model.cardMember +"\"," +
                //  "\"mobilePhone\":\""+ model.mobilePhone +"\"}";
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                JsonSerializer jsn = new JsonSerializer();
                jsn.Formatting = Formatting.Indented;
                var result = streamReader.ReadToEnd();
                streamReader.Close();
                var sr = new StringReader(result);

                var jr = new JsonTextReader(sr);
                //var u = jsn.Deserialize<Response>(jr);
                var u = JsonConvert.DeserializeObject(result);
                dynamic d = u;
                if (d.d != null)
                {
                    obj.CardLevel = d.d.CardLevel;
                    obj.CardNumber = d.d.CardNumber;
                    obj.ContactId = d.d.ContactId;
                    obj.FullName = d.d.Fullname;
                    obj.LastPointearned = d.d.LastPointEarned;
                    obj.LastPointEarnedDate = d.d.LastPointEarnedDate;
                    obj.NextExpireDate = d.d.NextExpireDate;
                    obj.NextExpiredPoint = d.d.NextExpiredPoint;
                    obj.TotalPoints = d.d.TotalPoints;
                    ViewData.Model = obj;
                }
                else
                {
                    return RedirectToAction(MVC.Index.Index());
                }
                //cardMemberResponse obje = (cardMemberResponse)JsonConvert.DeserializeObject(result,typeof(cardMemberResponse));
            }
         
            //using (HttpWebResponse httpWebResponse = httpWebRequest.GetResponse() as HttpWebResponse)
            //{
            //    if (httpWebResponse.StatusCode != HttpStatusCode.OK)
            //    {
            //        throw new Exception(string.Format("Server error (HTTP {0}: {1}).",
            //            httpWebResponse.StatusCode, httpWebResponse.StatusDescription));
            //    }
            //    var stream = httpWebResponse.GetResponseStream();
            //    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
            //    {
            //        JavaScriptSerializer js = new JavaScriptSerializer();
            //        var objText = reader.ReadToEnd();
            //        cardMemberResponse obj = (cardMemberResponse)js.Deserialize(objText, typeof(cardMemberResponse));
            //    }
            return View();
        }
        public virtual ActionResult point_balance(point_balance model)
        {
            if (model.startDate.Year == 0001 || model.endDate.Year == 0001)
            {
                model.endDate = DateTime.Now;
                model.startDate = model.endDate.AddDays(-7);
            }
            point_balance_response obj = new point_balance_response();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://117.102.84.181:8011/PIWSWeb.asmx/getPointBalance");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(model);
                //string json = "{\"cardMember\":\""+ model.cardMember +"\"," +
                //  "\"mobilePhone\":\""+ model.mobilePhone +"\"}";
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                JsonSerializer jsn = new JsonSerializer();
                jsn.Formatting = Formatting.Indented;
                var result = streamReader.ReadToEnd();
                streamReader.Close();
                var sr = new StringReader(result);

                var jr = new JsonTextReader(sr);
                //var u = jsn.Deserialize<Response>(jr);
                var u = JsonConvert.DeserializeObject(result);
                dynamic d = u;
                if (d.d != null)
                {
                    obj.base64string = d.d;
                    obj.startDate = model.startDate;
                    obj.endDate = model.endDate;
                    obj.contactId = model.contactId;
                    obj.CardNumber = model.CardNumber;
                    obj.CardLevel = model.CardLevel;
                    obj.FullName = model.FullName;
                    
                    ViewData.Model = obj;
                }
                else
                {
                    return RedirectToAction(MVC.Index.Index());
                }
            }
            return View();
        }
        public virtual ActionResult Contact()
        {
            return View();
        }
        [HttpPost]
        public virtual ActionResult Contact(privilegeContactModel model)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://117.102.84.181:8011/PIWSWeb.asmx/sendEmailContactUs");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(model);
                //string json = "{\"cardMember\":\""+ model.cardMember +"\"," +
                //  "\"mobilePhone\":\""+ model.mobilePhone +"\"}";
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                JsonSerializer jsn = new JsonSerializer();
                jsn.Formatting = Formatting.Indented;
                var result = streamReader.ReadToEnd();
                streamReader.Close();
                var sr = new StringReader(result);

                var jr = new JsonTextReader(sr);
                //var u = jsn.Deserialize<Response>(jr);
                var u = JsonConvert.DeserializeObject(result);
                dynamic d = u;
                if (d.d != null)
                {
                    return RedirectToAction(MVC.Index.Index());

                }
                else
                {
                    return RedirectToAction(MVC.Index.Index());
                }
            }
                
        }

    }
}