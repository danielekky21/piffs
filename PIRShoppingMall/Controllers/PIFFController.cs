﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.model;
using PIRShoppingMall.data;
using System.IO;

namespace PIRShoppingMall.Controllers
{
    public partial class PIFFController : Controller
    {
        // GET: PIFF

        public virtual ActionResult PIFFIndex()
        {
            data.vw_MasterTypeEvent piffMasterEventdata = new data.vw_MasterTypeEvent();            
            ViewBag.PIFFMasterdata = data.PIFFHeader.GetPIFFMasterEventData().Where(x => x.TypeEventID >= 8 && x.TypeEventID <= 10).ToList();
            ViewBag.PIFFDetailSubmission = data.PIFFHeader.GetPIFFMasterEventDataID(10);
            return View();
        }

        public virtual ActionResult PIFFMovie()
        {
            data.vw_MasterTypeEvent piffMasterEventdata = new data.vw_MasterTypeEvent();
            ViewBag.PIFFMasterdataMovie = data.PIFFHeader.GetPIFFMasterEventDataID(8);

            return View();
        }

        public virtual ActionResult PIFFWorkshop()
        {
            data.vw_MasterTypeEvent piffMasterEventdata = new data.vw_MasterTypeEvent();
            ViewBag.PIFFMasterdataWorkshop = data.PIFFHeader.GetPIFFMasterEventDataID(9);

            return View();

        }



        public virtual ActionResult MainIndex()
        {
            //Tidak di gunakan, hanya untuk coba coba

            //  data.spForgotPasswordPIFFA_Result piffdatatest = new data.spForgotPasswordPIFFA_Result();
            //data.Subscribe subscribe = new data.Subscribe(); // jika dari function
                     
            return View();

        }






        public virtual ActionResult PIFFMovies()
        {
            //Tidak di gunakan, hanya untuk coba coba

            //  data.spForgotPasswordPIFFA_Result piffdatatest = new data.spForgotPasswordPIFFA_Result();
            using (adminpir_plazaind_dbEntities1 db = new adminpir_plazaind_dbEntities1())
            {
                data.Subscribe subs = db.Subscribes.Where((x) => x.SubscribeID == 1).FirstOrDefault();
                ViewBag.subscribe = subs;
            }

            //using (adminpir_plazaind_dbEntities1 db = new adminpir_plazaind_dbEntities1())
            //{
            //    data.tabel_testDhal testdal = db.tabel_testDhal.Where((x) => x.id == 1).FirstOrDefault();
            //    ViewBag.testdal = testdal;
            //}

            ////List<sp_getPiffData_Result1> model = new List<sp_getPiffData_Result1>();
            ////model = CurrentDataContext.CurrentContext.sp_getPiffData("2019", 1).ToList();
            ////var movieScreen = model.Where(x => x.TypeEventID == 1).ToList();

            //ViewBag.testDhal2 = data.testDhal.getAllData().ToList();
            //ViewBag.testDhal3 = data.testDhal.getOneData(1);

            return View();

        }

                     

        public virtual ActionResult Index()
        {
            data.sp_LoginPIFFs_Result piffdata = new data.sp_LoginPIFFs_Result();
            if (!data.IsActiveMenu.getIsActiveMenuByID("PIFF").IsActive.GetValueOrDefault())
            {
                return RedirectToAction(Url.Action(MVC.Index.Index()));
            }
            List<sp_getPiffData_Result2> model = new List<sp_getPiffData_Result2>();
            model = CurrentDataContext.CurrentContext.sp_getPiffDataPIPC(DateTime.Now.Year.ToString(), 1).ToList();
            foreach (var item in model)
            {
                if (item.Image != null)
                {
                    item.Image = System.Configuration.ConfigurationManager.AppSettings["PiffImage"].ToString() + item.Image;
                }
            }


            ViewBag.PIFFData = data.PIFFHeader.GetPIFFHeaderData().ToList();
            if (Session["PIFFUser"] != null)
            {
                piffdata = (data.sp_LoginPIFFs_Result)Session["PIFFUser"];
            }
            ViewBag.piffs = piffdata;
            ViewData.Model = model;
            return View();
        }
        public virtual ActionResult ShortFilmRegulation()
        {
            return View();

        }
        public virtual ActionResult LogOut()
        {
            Session["PIFFUser"] = null;
            return RedirectToAction(MVC.PIFF.Index());
        }
        [HttpPost]
        public virtual JsonResult GetPIFFAjaxData(string year)
        {

            if (year == "Upcoming")
            {
                year = (DateTime.Now.Year + 1).ToString();
            }
            List<sp_getPiffData_Result1> model = new List<sp_getPiffData_Result1>();
            try
            {
                model = CurrentDataContext.CurrentContext.sp_getPiffData(year, 1).ToList();
            }
            catch (Exception e)
            {
               
                throw;
            }
         

            foreach (var item in model)
            {
                 if (item.Image != null)
                {
                    item.Image = System.Configuration.ConfigurationManager.AppSettings["PiffImage"].ToString() + item.Image;
                }
            }
            var movieScreen = model.Where(x => x.TypeEventID == 8).ToList();
            var workshopList = model.Where(x => x.TypeEventID == 9).ToList(); //movieClinic
            var data = new
            {
                movieScreen = movieScreen,
                workshopList = workshopList
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public virtual JsonResult ForgotPassword(string email)
        {
            Random rnd = new Random();
            string newPassword = rnd.Next(100000).ToString();
            string password = PIRShoppingMall.common.Security.DataEncryptionV2.HashStringSHA1(newPassword);
            data.spForgotPasswordPIFF_Result data = CurrentDataContext.CurrentContext.spForgotPasswordPIFF(email, password, newPassword).Single();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public virtual JsonResult LoginPIFF(string username, string password)
        {
            string pass = PIRShoppingMall.common.Security.DataEncryptionV2.HashStringSHA1(password);
            data.sp_LoginPIFFs_Result result = new data.sp_LoginPIFFs_Result();
            try
            {
                result = CurrentDataContext.CurrentContext.sp_LoginPIFFs(username, pass).AsQueryable().First();
                Session["PIFFUser"] = result;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                return Json(result, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public virtual JsonResult RegisterPIFFUser(model.PIFFRegistrationModel model)
        {
            try
            {
                string cryptedPass = PIRShoppingMall.common.Security.DataEncryptionV2.HashStringSHA1(model.UserPassword);

                var result = CurrentDataContext.CurrentContext.sp_registerPIFFUser(model.UserID, model.UserName, model.UserMail, model.Address, model.City, model.PostalCode, model.Country, model.Phone, model.Fax, cryptedPass, model.Gender);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("NotOK", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public virtual JsonResult UpdatePIFFUser(model.PIFFRegistrationModel model)
        {
            try
            {
                var result = CurrentDataContext.CurrentContext.sp_registerPIFFUser(model.UserMail, model.UserName, model.UserMail, model.Address, model.City, model.PostalCode, string.Empty, model.Phone, model.Fax, string.Empty, model.Gender);
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                return Json("NotOK", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public virtual JsonResult RegisterMovieScreening(string movie)
        {
            var userPIFF = (data.sp_LoginPIFFs_Result)Session["PIFFUser"];

            data.sp_RegisterScreening_Result data = CurrentDataContext.CurrentContext.sp_RegisterScreening(userPIFF.UserMail, int.Parse(movie)).Single();
            return Json(data.Result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public virtual JsonResult MovieClinicRegistration(model.PIFFMovieClinicModel model)
        {
            try
            {
                var userPIFF = (data.sp_LoginPIFFs_Result)Session["PIFFUser"];
                string fileName = "";
                string folder = "";
                string dir = "";
                string month = DateTime.Now.Month.ToString();
                string year = DateTime.Now.Year.ToString();
                string path = string.Empty;
                string UploadPath = string.Empty;
                string extension = string.Empty;

                foreach (string item in Request.Files)
                {

                    HttpPostedFileBase file = Request.Files[item] as HttpPostedFileBase;
                    fileName = userPIFF.UserMail + " " + Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/movieClinic/";
                    //folder = month + "-" + year + "/";
                    //dir = UploadPath + folder;
                    //if (!Directory.Exists(dir))
                    //{
                    //    Directory.CreateDirectory(dir);
                    //}

                    if (file.ContentLength == 0)
                        continue;
                    if (file.ContentLength > 0)
                    {
                        extension = Path.GetExtension(file.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        file.SaveAs(path);
                    }

                }

                bool award;
                bool edu;
                bool train;
                if (model.Award == "Yes")
                {
                    award = true;
                }
                else
                {
                    award = false;
                }
                if (model.Education == "Yes")
                {
                    edu = true;
                }
                else
                {
                    edu = false;
                }
                if (model.Training == "Yes")
                {
                    train = true;
                }
                else
                {
                    train = false;
                }

                CurrentDataContext.CurrentContext.sp_EventMovieClinics(userPIFF.UserMail, 42, model.Title_Film, model.Title_Film_other, model.date_completed, model.Role, model.Youtube_Link, award, model.MovClinAward, edu, null, model.MajorSmester, null, model.MajorGraduation, train, model.TrainingName, model.Reason);
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("NotOK", JsonRequestBehavior.AllowGet);
            }



        }
        [HttpPost]
        public virtual JsonResult ShortFilmRegistration(model.PIFFShortFilm model)
        {
            try
            {
                var userPIFF = (data.sp_LoginPIFFs_Result)Session["PIFFUser"];
                string fileName = "";
                string folder = "";
                string dir = "";
                string month = DateTime.Now.Month.ToString();
                string year = DateTime.Now.Year.ToString();
                string path = string.Empty;
                string UploadPath = string.Empty;
                string extension = string.Empty;

                foreach (string item in Request.Files)
                {

                    HttpPostedFileBase file = Request.Files[item] as HttpPostedFileBase;
                    fileName = userPIFF.UserMail + "_" + "16_" + Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/movieClinic/";
                    //folder = month + "-" + year + "/";
                    //dir = UploadPath + folder;
                    //if (!Directory.Exists(dir))
                    //{
                    //    Directory.CreateDirectory(dir);
                    //}

                    if (file.ContentLength == 0)
                        continue;
                    if (file.ContentLength > 0)
                    {
                        extension = Path.GetExtension(file.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        file.SaveAs(path);
                    }

                }
                bool festival = false;
                bool award = false;
                if (model.FilmFestival == "Yes")
                {
                    festival = true;
                }
                if (model.Film_Award == "Yes")
                {
                    award = true;
                }
                CurrentDataContext.CurrentContext.sp_EventShortFilmRegistration(userPIFF.UserMail, 16, model.Title_Film, model.Title_Film_other, model.date_completed, model.Film_duration, model.Film_Synopsis, festival, model.FilmFestivalNameDate, model.Film_Director, model.Film_Producer, model.Film_ScriptWriter, model.Film_DirectorOfPhoto, model.Film_Cast, model.Film_Editor, model.Film_ArtDirector, model.Film_MusicArrangement, model.Film_Others, model.Biograph, award, model.Film_AwardNameDate, model.Film_Format, model.OtherFormat, model.Color_Film, model.Film_Sound, model.Film_language, model.Film_Subtitle, model.Film_Link);
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                return Json("NotOK", JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public virtual JsonResult PIFFRegisterParticipant(model.PIFFRegisterParticipantModel model)
        {
            try
            {
                var result = CurrentDataContext.CurrentContext.sp_PIFFRegisterParticipant (model.EventCode , Int32.Parse(model.EventID), Int32.Parse(model.Amount), model.ParticipantName , model.ParticipantDoBirth,model.ParticipantEmail, model.ParticipantPhone,model.ParticipantName2,model.ParticipantDoBirth2, model.ParticipantEmail2, model.ParticipantPhone2);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("NotOK", JsonRequestBehavior.AllowGet);
            }
        }
    }
}