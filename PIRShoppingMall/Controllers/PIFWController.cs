﻿using PIRShoppingMall.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRShoppingMall.Controllers
{
    public partial class PIFWController : Controller
    {
        // GET: PIFW
        public virtual ActionResult Index()
        {
            data.sp_LoginPIFFs_Result piffdata = new data.sp_LoginPIFFs_Result();
            if (!data.IsActiveMenu.getIsActiveMenuByID("PIFF").IsActive.GetValueOrDefault())
            {
                return RedirectToAction(Url.Action(MVC.Index.Index()));
            }
            List<sp_getPiffData_Result1> model = new List<sp_getPiffData_Result1>();
            model = CurrentDataContext.CurrentContext.sp_getPiffData(DateTime.Now.Year.ToString(), 1).ToList();
            var eventlistDay = CurrentDataContext.CurrentContext.vw_PIFWEventList.Where(s => s.isActive == "Active").GroupBy(a => a.EventDate).Select(g => g.Key).ToList();
            var eventlist = CurrentDataContext.CurrentContext.vw_PIFWEventList.Where(s => s.isActive == "Active").ToList();
            foreach (var item in model)
            {
                if (item.Image != null)
                {
                    item.Image = System.Configuration.ConfigurationManager.AppSettings["PiffImage"].ToString() + item.Image;
                }
            }
            ViewBag.PIFFData = data.PIFFHeader.GetPIFFHeaderData().ToList();
            if (Session["PIFFUser"] != null)
            {
                piffdata = (data.sp_LoginPIFFs_Result)Session["PIFFUser"];
            }
            ViewBag.eventlistday = eventlistDay;
            ViewBag.eventlist = eventlist;
            ViewBag.piffs = piffdata;
            ViewData.Model = model;
            if (TempData["Result"] == null)
            {
                TempData["Result"] = string.Empty;
            }
           
            return View();
        }
        [HttpPost]
        public virtual ActionResult RegisterPIFW(PIRShoppingMall.data.Managed.PIFWModel model)
        {
            try
            {
                var res = PIRShoppingMall.data.CurrentDataContext.CurrentContext.sp_RegisterPIFW(model.first_name, model.last_name, int.Parse(model.age), model.email, int.Parse(model.show)).FirstOrDefault();
                if (res.Result == true)
                {
                    TempData["Result"] = "success";
                }
                else
                {
                    TempData["Result"] = "failed";
                }
            }
            catch (Exception e)
            {

                TempData["Result"] = "failed";
            }
            return RedirectToAction(MVC.PIFW.Index());
        }
        [HttpPost]
        public virtual ActionResult RegisterPIFWPIPC(PIRShoppingMall.data.Managed.PIFWModel model)
        {
            try
            {
                var res = PIRShoppingMall.data.CurrentDataContext.CurrentContext.sp_RegisterPIFWPIPC(model.first_name, model.last_name, int.Parse(model.age), model.email, int.Parse(model.show),model.pipc).FirstOrDefault();
                if (res.Result == true)
                {
                    TempData["Result"] = "success";
                }
                else
                {
                    TempData["Result"] = "failed";
                }
            }
            catch (Exception e)
            {

                TempData["Result"] = "failed";
            }
            return RedirectToAction(MVC.PIFW.PIPC());
        }
        public virtual JsonResult ValidatePIPC(string pipc)
        {
            bool? res = false;
            if (pipc.Length == 12)
            {
                var result = CurrentDataContext.CurrentContext.sp_ValidatePIPCCard(pipc).FirstOrDefault();
                res = result;
            }
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult PIPC()
        {
            data.sp_LoginPIFFs_Result piffdata = new data.sp_LoginPIFFs_Result();
            if (!data.IsActiveMenu.getIsActiveMenuByID("PIFF").IsActive.GetValueOrDefault())
            {
                return RedirectToAction(Url.Action(MVC.Index.Index()));
            }
            List<sp_getPiffData_Result2> model = new List<sp_getPiffData_Result2>();
            model = CurrentDataContext.CurrentContext.sp_getPiffDataPIPC(DateTime.Now.Year.ToString(), 1).ToList();
            var eventlistDay = CurrentDataContext.CurrentContext.vw_PIFWEventList_PIPC.Where(s => s.isActive == "Active").GroupBy(a => a.EventDate).Select(g => g.Key).ToList();
            var eventlist = CurrentDataContext.CurrentContext.vw_PIFWEventList_PIPC.Where(s => s.isActive == "Active").ToList();
            foreach (var item in model)
            {
                if (item.Image != null)
                {
                    item.Image = System.Configuration.ConfigurationManager.AppSettings["PiffImage"].ToString() + item.Image;
                }
            }
            ViewBag.PIFFData = data.PIFFHeader.GetPIFFHeaderData().ToList();
            if (Session["PIFFUser"] != null)
            {
                piffdata = (data.sp_LoginPIFFs_Result)Session["PIFFUser"];
            }
            ViewBag.eventlistday = eventlistDay;
            ViewBag.eventlist = eventlist;
            ViewBag.piffs = piffdata;
            ViewData.Model = model;
            if (TempData["Result"] == null)
            {
                TempData["Result"] = string.Empty;
            }

            return View();
        }
    }
}