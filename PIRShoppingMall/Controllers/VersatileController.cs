﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;

namespace PIRShoppingMall.Controllers
{
    public partial class VersatileController : Controller
    {
        // GET: Versatile
        public virtual ActionResult Index(int id)
        {
            data.banner data = banner.GetByID(id);
            List<string> imageUrl = new List<string>();
            string[] Split;
            if (data.miniGalleryImg != null)
            {
                Split = data.miniGalleryImg.Split(';');
                List<string> ListSplit = Split.ToList();
                for (int i = 0; i < ListSplit.Count; i++)
                {
                    if (string.IsNullOrEmpty(Split[i]))
                    {
                        ListSplit.RemoveAt(i);
                    }
                }
                foreach (var a in ListSplit)
                {
                    imageUrl.Add(image.GetByID(int.Parse(a)).image_image);
                }
            }

            ViewBag.isActivePIFF = IsActiveMenu.getIsActiveMenuByID("PIFF").IsActive;

            ViewData.Model = data;
            ViewBag.imageList = imageUrl;
            return View();
        }
    }
}