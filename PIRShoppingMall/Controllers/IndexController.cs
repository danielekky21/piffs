﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;
using PIRShoppingMall.model;
using PIRShoppingMall.common;
using System.Configuration;

namespace PIRShoppingMall.Controllers
{
    public partial class IndexController : Controller
    {
        // GET: Home
        public virtual ActionResult Index()
        {
            List<string> imgPath = new List<string>();
            List<blog_image> blogSlider = new List<blog_image>();
            ViewBag.Banner = banner.GetOnlyActive().OrderBy(x => x.banner_sort).ToList();
            ViewBag.BannerExtend = banner.GetIsExtend().OrderBy(x => x.banner_sort).ToList();
            ViewBag.Events = @event.GetAllOnlyActive().OrderBy(x => x.event_sort).ToList();
            ViewBag.BlogHead = other_blog_data.GetBlogHeaderActive();
            ViewBag.isActivePIFF = data.IsActiveMenu.getIsActiveMenuByID("PIFF").IsActive;
            ViewBag.FAQ = FAQTb.GetFirstActiveFAQheader();
            ViewBag.siteConfig = ConfigurationManager.AppSettings["SiteURL"];
            if (FAQTb.GetFirstActiveFAQheader() != null)
            {
                ViewBag.FAQHeader = FAQTb.GetFirstActiveFAQheader().FAQHeader;
            }
            List<blog> blogitem = blog.GetAllBlogActive().OrderBy(x => x.blog_sort).ToList();
            ViewBag.BlogItem = blogitem;
            foreach (var item in blogitem)
            {
                blogSlider.AddRange(blog_image.GetByBlogID(item.blog_id));
            }
            ViewBag.blogSlider = blogSlider;
            var news = newsletter.GetFirstEnabledItem();
            ViewBag.HighLight = news;
            ViewBag.reward = Reward.GetAllActive().OrderBy(x => x.RewardIndex).ToList();
            var imageid = newsletter_slider_image.GetByNewsletterID(news.newsletter_id).ToList();
            foreach (var imgIDCollection in imageid)
            {
                imgPath.Add(image.GetImagePathByID(imgIDCollection.image_id));
            }
            ViewBag.HighLigthsliderPath = imgPath;
            ViewBag.PromoHeader = promo_front.GetAllPromoFront().Where(x => x.fpromo_status == 1).OrderBy(x => x.fpromo_sort).ToList();
            ViewBag.PromoItem = promo_back.GetPromoItemActive().OrderBy(x => x.promo_front.fpromo_sort).ToList();
            ViewBag.Opening = opening.GetAllActive().OrderBy(x => x.opening_sort).ToList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult InsertMessage(CustomContact model)
        {
            ContactTb contactData = new ContactTb();
            string contactto = string.Empty;
            contactData.Email = model.email;
            contactData.Name = model.name;
            contactData.Message = model.message;
            contactData.Phone = model.phone;
            foreach (var item in model.reciever)
            {
                contactto += item + " ";
            }
            contactData.ContactTo = contactto;
            contactData.insert();
            return RedirectToAction(MVC.Index.Index());
        }
        public virtual ActionResult BlogArchive(string month, int year)
        {
            int convertedMonth = GlobalFunction.ConvertMonthNameIntoInt(month);
            List<blog_image> blogSlider = new List<blog_image>();
            var blogHead = other_blog_data.GetBlogHeaderByMonthAndYear(convertedMonth, year);
            if (blogHead != null)
            {
                ViewBag.BlogHead = blogHead;
            }
            else
            {
                blogHead = new other_blog_data();
                blogHead.blog_date = DateTime.ParseExact("01-" + month + "-" + year, "dd-MMMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                ViewBag.BlogHead = blogHead;
            }
            var blogItem = blog.GetBlogByMonthAndYear(convertedMonth, year).ToList();
            ViewBag.BlogItem = blogItem;
            ViewBag.Month = month;
            foreach (var item in blogItem)
            {
                blogSlider.AddRange(blog_image.GetByBlogID(item.blog_id));
            }
            ViewBag.blogSlider = blogSlider;
            return View();
        }
        [HttpPost]
        public virtual JsonResult GetStoreLevel(string cat_id, string level)
        {
            var tenant = CurrentDataContext.CurrentContext.vw_Tenant.Where(x => x.TenantWebCategory == cat_id && x.ShopFloor.Contains(level)).ToList();
            List<stores> ListStore = new List<stores>();
            foreach (var item in tenant)
            {
                stores store = new stores();
                store.id_store = item.TenantID.ToString();
                store.id_ref_store_category = item.TenantWebCategory;
                store.kav = item.ShopUnitNo;
                store.store = item.ShopName;
                if (item.ShopFloor == "LB")
                {
                    store.lantai = item.ShopFloor;
                }
                else
                {
                    store.lantai = item.ShopFloor.Split(' ')[1];
                }
                ListStore.Add(store);
            }
            return Json(ListStore, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public virtual JsonResult SearchStore(string category, string keyword)
        {
            List<vw_Tenant> tenant = new List<vw_Tenant>();
            if (category != "0")
            {
                tenant = CurrentDataContext.CurrentContext.vw_Tenant.Where(x => x.TenantWebCategory == category && x.ShopName.Contains(keyword)).ToList();
            }
            else
            {
                tenant = CurrentDataContext.CurrentContext.vw_Tenant.Where(x => x.ShopName.Contains(keyword)).ToList();
            }
            List<stores> ListStore = new List<stores>();
            foreach (var item in tenant)
            {
                stores store = new stores();
                store.id_store = item.TenantID.ToString();
                store.id_ref_store_category = item.TenantWebCategory;
                store.kav = item.ShopUnitNo;
                store.store = item.ShopName;
                if (item.ShopFloor == "LB" || item.ShopFloor == "A" || item.ShopFloor == "P2" || item.ShopFloor == "P3")
                {
                    store.lantai = item.ShopFloor;
                }
                else
                {
                    store.lantai = item.ShopFloor.Split(' ')[1];
                }
                ListStore.Add(store);
            }
            return Json(ListStore, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public virtual ActionResult InsertSubscriberEmail(string emailAddress)
        {
            if (!string.IsNullOrEmpty(emailAddress))
            {
                Subscribe sub = new Subscribe();
                sub.SubscribeEmail = emailAddress;
                sub.CreatedDate = DateTime.Now;
                sub.InsertSave<Subscribe>();
            }
           
            return RedirectToAction(MVC.Index.Index());
        }
    }
}