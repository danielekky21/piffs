﻿using PIRShoppingMall.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;
using System.Globalization;

namespace PIRShoppingMall.Controllers
{
    public partial class ArchiveController : Controller
    {
        // GET: Archive
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult highlights(string month, string year)
        {
            List<string> imgPath = new List<string>();
            int convertedMonth = GlobalFunction.ConvertMonthNameIntoInt(month);
            newsletter data = newsletter.GetNewsLetterByMonth(convertedMonth);
            if (data == null)
            {
                data = new newsletter();
                data.newsletter_date = DateTime.ParseExact("01-" + month + "-" + year, "dd-MMMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
            var imageid = newsletter_slider_image.GetByNewsletterID(data.newsletter_id).ToList();
            foreach (var imgIDCollection in imageid)
            {
                imgPath.Add(image.GetImagePathByID(imgIDCollection.image_id));
            }
            ViewBag.HighLigthsliderPath = imgPath;
            ViewBag.HighLigthsData = data;
            return View();
        }
        public virtual ActionResult blog(string month, int year)
        {
            int convertedMonth = GlobalFunction.ConvertMonthNameIntoInt(month);
            List<blog_image> blogSlider = new List<blog_image>();
            var blogHead = other_blog_data.GetBlogHeaderByMonthAndYear(convertedMonth, year);
            if (blogHead != null)
            {
                ViewBag.BlogHead = blogHead;
            }
            else
            {
                blogHead = new other_blog_data();
                blogHead.blog_date = DateTime.ParseExact("01-" + month + "-" + year, "dd-MMMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                ViewBag.BlogHead = blogHead;
            }
            var blogItem = data.blog.GetBlogByMonthAndYear(convertedMonth, year).ToList();
            ViewBag.BlogItem = blogItem;
            ViewBag.Month = month;
            foreach (var item in blogItem)
            {
                blogSlider.AddRange(blog_image.GetByBlogID(item.blog_id));
            }
            ViewBag.blogSlider = blogSlider;
            return View();
        }
        public virtual ActionResult Event(string month, int year)
        {
            
            int convertedMonth = GlobalFunction.ConvertMonthNameIntoInt(month);
            CultureInfo ci = new CultureInfo("en-US");
            DateTime currentTime = DateTime.ParseExact("1/" + convertedMonth + "/" + year,"d/M/yyyy",ci);

            List<string> listImg = new List<string>();
            List<@event> eventImage = @event.GetEventByMonthAndYear(convertedMonth, year).ToList();
            if (eventImage != null)
            {

                foreach (var item in eventImage)
                {
                    if (item.event_image.GetValueOrDefault() != 0)
                    {
                        listImg.Add(image.GetImagePathByID(item.event_image.GetValueOrDefault()));
                    }
                  
                }
                //eventImage = new @event();
                //eventImage.event_date = DateTime.ParseExact("01-" + month + "-" + year, "dd-MMMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
            ViewBag.currentTime = currentTime;
            ViewBag.imgList = listImg;
            ViewBag.month = month;
            ViewBag.year = year.ToString();
            return View();
        }
    }
}