﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PIRShoppingMall.Authorize
{
    public class PlazaindonesiaAuthorizationAttribute : AuthorizeAttribute
    {
        private AuthorizationContext _currentAuthContext;

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            _currentAuthContext = filterContext;

            HttpCookie cookie = filterContext.RequestContext.HttpContext.Request.Cookies["PlazaindonesiaManagementCookie"];
            string _UserID = string.Empty;
            if (cookie != null)
            {

                _UserID = cookie["user"];
                int UserID = int.Parse(_UserID);
                data.user _User = data.user.GetByID(UserID);
                if (_User == null || _User.status == 0)
                {
                    RedirectToLoginPage();
                }


            }
            else
            {
                RedirectToLoginPage();
            }

        }

        private void RedirectToLoginPage()
        {
            var returnUrl = _currentAuthContext.HttpContext.Request.Url.AbsolutePath;
            var redirectResult = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "Login" }));
            _currentAuthContext.Result = redirectResult;
        }
        private void RedirectToLandingPage()
        {
            var returnUrl = _currentAuthContext.HttpContext.Request.Url.AbsolutePath;
            var redirectResult = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Dashboard", action = "LandingPage" }));
            _currentAuthContext.Result = redirectResult;
        }
    }
}