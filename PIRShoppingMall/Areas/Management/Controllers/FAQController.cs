﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;
namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class FAQController : Controller
    {
        // GET: Management/FAQ
        public virtual ActionResult List()
        {
            ViewData.Model = FAQTb.GetAllFAQHeader().OrderByDescending(x => x.CreatedDate).ToList();
            return View();
        }
        public virtual ActionResult AddEditFAQ(int id)
        {
            if (id != 0)
            {
                ViewData.Model = FAQTb.GetFAQByID(id);
            }
            else
            {
                ViewData.Model = new FAQTb();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEditFAQ(FAQTb model)
        {
            if (model.FAQID != 0)
            {
                FAQTb data = FAQTb.GetFAQByID(model.FAQID);
                if (data != null)
                {
                    data.FAQHeader = model.FAQHeader;
                    data.IsActive = model.IsActive;
                    data.Update();
                }
               
            }
            else
            {
                model.CreatedDate = DateTime.Now;
                model.IsActive = model.IsActive;
                model.Insert();
            }
            return RedirectToAction(MVC.Management.FAQ.List());
        }
        public virtual ActionResult ListFAQItem(int id)
        {
            ViewData.Model = FAQItemTb.GetAllFAQItem(id).OrderByDescending(x => x.CreatedDate).ToList();
            ViewBag.FAQID = id;
            return View();
        }
        public virtual ActionResult FAQItemAddEdit(int id,int faqid)
        {
            if (id != 0)
            {
                ViewData.Model = FAQItemTb.GetFAQItemByID(id,faqid);
                
            }
            else
            {
                FAQItemTb item = new FAQItemTb();
                item.FAQID = faqid;
                ViewData.Model = item;

            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult FAQItemAddEdit(FAQItemTb model)
        {
            if (model.FAQItemId != 0)
            {
                FAQItemTb data = FAQItemTb.GetFAQItemByID(model.FAQItemId,model.FAQID.GetValueOrDefault());
                if (data != null)
                {
                    data.Question = model.Question;
                    data.Answer = model.Answer;
                    data.IsActive = model.IsActive;
                    data.Update();
                }

            }
            else
            {
                FAQItemTb newItem = new FAQItemTb();
                newItem.FAQID = model.FAQID;
                newItem.Question = model.Question;
                newItem.Answer = model.Answer;
                newItem.IsActive = model.IsActive;
                newItem.Insert();
            }
            return RedirectToAction(MVC.Management.FAQ.ListFAQItem(model.FAQID.GetValueOrDefault()));
        }

    }
}