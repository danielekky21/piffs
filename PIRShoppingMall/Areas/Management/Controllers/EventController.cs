﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;
using PIRShoppingMall.Authorize;
using PIRShoppingMall.model;

namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class EventController : Controller
    {
        // GET: Management/Event
        [PlazaindonesiaAuthorization]
        public virtual ActionResult List()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewData.Model = data.@event.GetAllActive().OrderByDescending(x => x.status).ThenBy(x => x.event_sort).ToList();
            return View();
        }
        public virtual ActionResult AddEdit(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewBag.imagelist = data.image.GetAllActive().OrderByDescending(x => x.date_added).ToList();
            if (id != 0)
            {
                data.@event events = data.@event.GetByID(id);
                ViewData.Model = events;
            }
            else
            {
                data.@event events = new data.@event();
                events.event_sort = 1;
                events.event_date = DateTime.Now.Date;
                ViewData.Model = events; 
                
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(EventModel model)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (model.event_id != 0)
            {
                data.@event events = data.@event.GetByID(model.event_id);
                if (events != null)
                {
                    events.event_image = model.event_image;
                    events.event_date = model.event_date;
                    events.event_sort = model.event_sort;
                    events.status = model.status;
                }
                events.Update();
            }
            else
            {
                data.@event newEvents = new data.@event();
                if (model.event_image == null)
                {
                    model.event_image = 0;
                }
                else
                {
                    newEvents.event_image = model.event_image;
                }
                newEvents.event_date = model.event_date;
                newEvents.event_sort = model.event_sort;
                newEvents.status = model.status;
                newEvents.Insert();
            }
            return RedirectToAction(MVC.Management.Event.List());
        }
        public virtual ActionResult Delete(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (id != 0)
            {
                @event eventModel = @event.GetByID(id);
                if (eventModel != null)
                {
                    eventModel.Delete();
                }
            }
            return RedirectToAction(MVC.Management.Event.List());
        }
    }
}