﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.model;
using PIRShoppingMall.data;
using System.IO;
namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class NewsletterController : Controller
    {
        // GET: Management/Newsletter
        public virtual ActionResult List()
        {
            if (Session["UserId"] == null)
            {
              return  RedirectToAction(MVC.Management.Home.Login());
            }
            ViewData.Model = data.newsletter.GetAllActive().OrderByDescending(x => x.status).ToList();
            return View();
        }
        public virtual ActionResult AddEdit(int id)
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction(MVC.Management.Home.Login());
            }
            ViewBag.imagelist = data.image.GetAllActive().OrderByDescending(x => x.date_added).ToList();

            if (id != 0)
            {
                newsletter news = newsletter.GetByID(id);
                var sliderdata = data.newsletter_slider_image.GetByNewsletterID(id).ToList();
                ViewBag.checkedList = newsletter_slider_image.callImageId(sliderdata);
                ViewData.Model = news;
            }
            else
            {

                newsletter item = new newsletter();
                item.newsletter_date = DateTime.Now;
                ViewData.Model = item;
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(HttpPostedFileBase component1, HttpPostedFileBase component2, HttpPostedFileBase component3, HttpPostedFileBase component4, NewsletterModel model, List<string> imageSlider)
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction(MVC.Management.Home.Login());
            }
            string fileName = "";
            string folder = "";
            string dir = "";
            string path = string.Empty;
            string UploadPath = string.Empty;
            string extension = string.Empty;
            if (model.newsletter_id == 0)
            {
                if (component1 != null && component1.ContentLength > 0)
                {
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/cmsimage/";
                    if (component1.ContentLength > 0)
                    {
                        extension = Path.GetExtension(component1.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        component1.SaveAs(path);
                        model.newsletter_first_image = UploadPath + fileName + extension;
                    }

                }
                path = string.Empty;
                if (component2 != null && component2.ContentLength > 0)
                {
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/cmsimage/";
                    if (component2.ContentLength > 0)
                    {
                        extension = Path.GetExtension(component2.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        component2.SaveAs(path);
                        model.newsletter_second_image = UploadPath + fileName + extension;
                    }

                }
                path = string.Empty;
                if (component3 != null && component3.ContentLength > 0)
                {
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/cmsimage/";
                    if (component3.ContentLength > 0)
                    {
                        extension = Path.GetExtension(component2.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        component3.SaveAs(path);
                        model.newsletter_third_image = UploadPath + fileName + extension;
                    }

                }
                path = string.Empty;
                if (component4 != null && component4.ContentLength > 0)
                {
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/cmsimage/";
                    if (component4.ContentLength > 0)
                    {
                        extension = Path.GetExtension(component2.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        component4.SaveAs(path);
                        model.newsletter_fourth_image = UploadPath + fileName + extension;
                    }

                }
                data.newsletter news = new data.newsletter();
                news.newsletter_first_image = model.newsletter_first_image;
                news.newsletter_second_image = model.newsletter_second_image;
                news.newsletter_second_title = model.newsletter_second_title;
                news.newsletter_second_desc = model.newsletter_second_desc;
                news.newsletter_second_url = model.newsletter_second_url;
                news.newsletter_third_image = model.newsletter_third_image;
                news.newsletter_fourth_image = model.newsletter_fourth_image;
                news.newsletter_fourth_desc = model.newsletter_fourth_desc;
                news.newsletter_date = model.newsletter_date;
                news.status = model.status;
                news.Insert();

                if (imageSlider != null)
                {
                    foreach (var item in imageSlider)
                    {
                        data.newsletter_slider_image slider = new newsletter_slider_image();
                        slider.newsletter_id = news.newsletter_id;
                        slider.image_id = int.Parse(item);
                        slider.Insert();
                    }
                }

            }
            else
            {
                if (component1 != null && component1.ContentLength > 0)
                {
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/cmsimage/";
                    if (component1.ContentLength > 0)
                    {
                        extension = Path.GetExtension(component1.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        component1.SaveAs(path);
                        model.newsletter_first_image = UploadPath + fileName + extension;
                    }

                }
                path = string.Empty;
                if (component2 != null && component2.ContentLength > 0)
                {
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/cmsimage/";
                    if (component2.ContentLength > 0)
                    {
                        extension = Path.GetExtension(component2.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        component2.SaveAs(path);
                        model.newsletter_second_image = UploadPath + fileName + extension;
                    }

                }
                path = string.Empty;
                if (component3 != null && component3.ContentLength > 0)
                {
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/cmsimage/";
                    if (component3.ContentLength > 0)
                    {
                        extension = Path.GetExtension(component3.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        component3.SaveAs(path);
                        model.newsletter_third_image = UploadPath + fileName + extension;
                    }

                }
                path = string.Empty;
                if (component4 != null && component4.ContentLength > 0)
                {
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/cmsimage/";
                    if (component4.ContentLength > 0)
                    {
                        extension = Path.GetExtension(component4.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        component4.SaveAs(path);
                        model.newsletter_fourth_image = UploadPath + fileName + extension;
                    }

                }
                newsletter current = newsletter.GetByID(model.newsletter_id);
                current.newsletter_first_image = model.newsletter_first_image;
                current.newsletter_second_image = model.newsletter_second_image;
                current.newsletter_second_title = model.newsletter_second_title;
                current.newsletter_second_desc = model.newsletter_second_desc;
                current.newsletter_second_url = model.newsletter_second_url;
                current.newsletter_third_image = model.newsletter_third_image;
                current.newsletter_fourth_image = model.newsletter_fourth_image;
                current.newsletter_fourth_desc = model.newsletter_fourth_desc;
                current.newsletter_date = model.newsletter_date;
                current.status = model.status;
                current.Update();
                List<newsletter_slider_image> currentSlider = newsletter_slider_image.GetByNewsletterID(model.newsletter_id).ToList();
                if (currentSlider != null)
                {
                    foreach (var item in currentSlider)
                    {
                        item.Remove();
                    }
                }

                if (imageSlider != null)
                {
                    foreach (var item in imageSlider)
                    {
                        data.newsletter_slider_image slider = new newsletter_slider_image();
                        slider.newsletter_id = model.newsletter_id;
                        slider.image_id = int.Parse(item);
                        slider.Insert();
                    }
                }
            }
            return RedirectToAction(MVC.Management.Newsletter.List());
        }
        public virtual ActionResult Delete(int id)
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction(MVC.Management.Home.Login());
            }
            if (id != 0)
            {
                List<data.newsletter_slider_image> slider = data.newsletter_slider_image.GetByNewsletterID(id).ToList();

                if (slider != null)
                {
                    foreach (var item in slider)
                    {
                        item.Remove();
                    }
                }
                data.newsletter newsletter = data.newsletter.GetByID(id);
                newsletter.Delete();
            }
            return RedirectToAction(MVC.Management.Newsletter.List());
        }
    }
}