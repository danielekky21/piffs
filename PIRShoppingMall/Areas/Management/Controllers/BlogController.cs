﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;
using PIRShoppingMall.model;

namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class BlogController : Controller
    {
        // GET: Management/Blog
        public virtual ActionResult Index()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            return View();
        }
        public virtual ActionResult List()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewData.Model = other_blog_data.GetBlogHeaderData().OrderByDescending(x => x.blog_status).ToList();
            return View();

        }
        public virtual ActionResult AddEdit(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (id != 0)
            {
                ViewData.Model = other_blog_data.GetBlogHeaderByID(id);

            }
            else
            {

                other_blog_data blogHeader = new other_blog_data();
                blogHeader.blog_date = DateTime.Now;
                ViewData.Model = blogHeader;
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(BlogHeader Model)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (Model.other_blog_id != 0)
            {
                other_blog_data bloghead = other_blog_data.GetBlogHeaderByID(Model.other_blog_id);
                if (bloghead != null)
                {
                    bloghead.blog_title = Model.blog_title;
                    bloghead.blog_desc = Model.blog_desc;
                    bloghead.blog_date = Model.blog_date;
                    bloghead.blog_status = Model.blog_status;
                }
                bloghead.Update();
            }
            else
            {
                other_blog_data newHead = new other_blog_data();
                newHead.blog_title = Model.blog_title;
                newHead.blog_desc = Model.blog_desc;
                newHead.blog_date = Model.blog_date;
                newHead.blog_status = Model.blog_status;
                newHead.Insert();
            }
            return RedirectToAction(MVC.Management.Blog.List());
        }
        public virtual ActionResult Delete(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (id != 0)
            {
                other_blog_data blogHeadItem = other_blog_data.GetBlogHeaderByID(id);
                if (blogHeadItem != null)
                {
                    blogHeadItem.Delete();
                }
            }
            return RedirectToAction(MVC.Management.Blog.List());
        }
        public virtual ActionResult BlogitemList()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewData.Model = blog.GetAllBlogItem().OrderByDescending(x => x.status).ToList();
            return View();
        }
        public virtual ActionResult BlogItemAddEdit(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewBag.imagelist = data.image.GetAllActive().OrderByDescending(x => x.date_added).ToList();
            if (id != 0)
            {
                blog blogitem = blog.GetBlogItemByID(id);
                var sliderdata = blog_image.GetByBlogID(id).ToList();
                ViewBag.checkedList = blog_image.callImageId(sliderdata);
                ViewData.Model = blogitem;
            }
            else
            {
                blog newBlog = new blog();
                newBlog.blog_date = DateTime.Now;
                ViewData.Model = newBlog;
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult BlogItemAddEdit(BlogItem model)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (model.blog_id != 0)
            {
                blog blogData = blog.GetBlogItemByID(model.blog_id);
                blogData.blog_title = model.blog_title;
                blogData.blog_desc = model.blog_desc;
                blogData.blog_date = model.blog_date;
                blogData.blog_sort = model.blog_sort;
                blogData.image_id = model.image_id;
                blogData.status = model.status;
                blogData.Update();
                List<blog_image> currentSlider = blog_image.GetByBlogID(model.blog_id).ToList();
                if (currentSlider != null)
                {
                    foreach (var item in currentSlider)
                    {
                        item.Delete();
                    }
                }
                if (model.imageSlider != null)
                {
                    foreach (var item in model.imageSlider)
                    {
                        data.blog_image slider = new blog_image();
                        slider.blog_id = blogData.blog_id;
                        slider.image_id = int.Parse(item);
                        slider.Insert();
                    }
                }
            }
            else
            {
                data.blog blogs = new data.blog();
                blogs.blog_title = model.blog_title;
                blogs.blog_desc = model.blog_desc;
                blogs.blog_date = model.blog_date;
                blogs.blog_sort = model.blog_sort;
                blogs.date_added = DateTime.Now;
                blogs.image_id = model.image_id;
                blogs.Insert();
                if (model.imageSlider != null)
                {
                    foreach (var item in model.imageSlider)
                    {
                        data.blog_image slider = new data.blog_image();
                        slider.blog_id = blogs.blog_id;
                        slider.image_id = int.Parse(item);
                        slider.Insert();
                    }
                }
            }
            return RedirectToAction(MVC.Management.Blog.BlogitemList());

        }
        public virtual ActionResult BlogItemDelete(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (id != 0)
            {
                List<blog_image> imageList = blog_image.GetByBlogID(id).ToList();
                if (imageList != null)
                {
                    foreach (var item in imageList)
                    {
                        item.Remove();
                    }
                }
                blog blogs = blog.GetBlogItemByID(id);
                blogs.Delete();
            }
            return RedirectToAction(MVC.Management.Blog.BlogitemList());
        }
    }
}