﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.Authorize;
namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class DashboardController : Controller
    {
        // GET: Management/Dashboard
        [PlazaindonesiaAuthorization]
        public virtual ActionResult LandingPage()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            return View();
        }
    }
}