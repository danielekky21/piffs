﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;

namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class RewardController : Controller
    {
        // GET: Management/Reward
        public virtual ActionResult List()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewData.Model = Reward.GetAll().OrderByDescending(x => x.IsActive).ThenBy(x => x.RewardIndex).ToList();
            return View();
        }
        public virtual ActionResult AddEdit(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewBag.imagelist = data.image.GetAllActive().OrderByDescending(x => x.date_added).ToList();
            if (id != 0)
            {
                ViewData.Model = Reward.GetItemByID(id);

            }
            else
            {
                ViewData.Model = new Reward();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(Reward model)
        {
            if (model.RewardID != 0)
            {
                Reward data = Reward.GetItemByID(model.RewardID);
                if (data != null)
                {
                    data.RewardImage = model.RewardImage;
                    data.RewardIndex = model.RewardIndex;
                    data.RewardName = model.RewardName;
                    data.IsActive = model.IsActive;
                    data.RewardPoint = model.RewardPoint;
                    data.Update();
                }
            }
            else
            {
                model.CreatedDate = DateTime.Now;
                model.Insert();
            }
            return RedirectToAction(MVC.Management.Reward.List());
        }
    }
}