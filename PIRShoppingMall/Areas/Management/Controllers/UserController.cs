﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PIRShoppingMall.data;
using System.Web.Mvc;
using PIRShoppingMall.common;

namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class UserController : Controller
    {
        // GET: Management/User
        public virtual ActionResult Index()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewData.Model = user.GetAllActiveUser().OrderByDescending(x => x.date_added).ToList();
            return View();
        }
        public virtual ActionResult AddEdit(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (id == 0)
            {
                ViewData.Model = new user();
            }
            else
            {
                user model = user.GetByID(id);
                model.user_password = common.Security.DataEncription.Decrypt(model.user_password);
                ViewData.Model = model;
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(user model)
        {
            if (model.user_id != 0)
            {
                user current = user.GetByID(model.user_id);
                current.user_name = model.user_name;
                current.user_username = model.user_username;
                current.user_password = common.Security.DataEncription.Encrypt(model.user_password);
                current.date_modified = DateTime.Now;
                current.Update();
            }
            else
            {
                model.date_added = DateTime.Now;
                model.date_modified = DateTime.Now;
                model.add_user = 1;
                model.last_modify_user = 1;
                model.status = 1;
                model.user_password = common.Security.DataEncription.Encrypt(model.user_password);
                model.Insert();
            }
            return RedirectToAction(MVC.Management.User.Index());
        }
        public virtual ActionResult Delete(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (id != 0)
            {
                user userDelete = user.GetByID(id);
                userDelete.status = 0;
                userDelete.date_modified = DateTime.Now;
                userDelete.Update();
            }
            return RedirectToAction(MVC.Management.User.Index());
        }

    }
}