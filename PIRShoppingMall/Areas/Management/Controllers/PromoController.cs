﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;
using System.IO;
namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class PromoController : Controller
    {
        // GET: Management/Promo
        public virtual ActionResult List()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewData.Model = promo_front.GetAllPromoFront().OrderByDescending(x => x.fpromo_status).ThenBy(x => x.fpromo_sort).ToList();
            return View();
        }
        public virtual ActionResult Edit(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (id != 0)
            {
                data.promo_front promoData = data.promo_front.GetPromoHeadByID(id);
                ViewData.Model = promoData;
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult Edit(promo_front model)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (model.fpromo_id != 0)
            {
                promo_front data = promo_front.GetPromoHeadByID(model.fpromo_id);
                data.fpromo_name = model.fpromo_name;
                data.fpromo_sort = model.fpromo_sort;
                data.fpromo_status = model.fpromo_status;
                data.Update();
            }
            return RedirectToAction(MVC.Management.Promo.List());
        }
        public virtual ActionResult PromoItemList()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewData.Model = promo_back.GetAllPromoItem().OrderByDescending(x => x.bpromo_status).ToList();
            return View();
        }
        public virtual ActionResult PromoItemAddEdit(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            List<SelectListItem> items = new List<SelectListItem>();
            List<SelectListItem> StatusItem = new List<SelectListItem>();
            var promo = promo_front.GetAllPromoFront().OrderBy(x => x.fpromo_sort).ToList();
            foreach (var item in promo)
            {
                items.Add(new SelectListItem { Text = "(" + item.fpromo_sort + ")" + item.fpromo_name, Value = item.fpromo_id.ToString() });
            }
            ViewBag.promoType = items;
            StatusItem.Add(new SelectListItem { Text = "enable" , Value = "1" });
            StatusItem.Add(new SelectListItem { Text = "disable", Value = "0" });

            ViewBag.Stat = StatusItem;
            if (id != 0)
            {
                promo_back dataPromo = promo_back.GetPromoById(id);
                ViewData.Model = dataPromo;

            }
            else
            {
                ViewData.Model = new promo_back();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult PromoItemAddEdit(promo_back data, HttpPostedFileBase component1)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            string fileName = "";
            string folder = "";
            string dir = "";
            string path = string.Empty;
            string UploadPath = string.Empty;
            string extension = string.Empty;
            if (data.bpromo_id == 0)
            {
                promo_back model = new promo_back();
                if (component1 != null && component1.ContentLength > 0)
                {
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/cmsimage/";
                    if (component1.ContentLength > 0)
                    {
                        extension = Path.GetExtension(component1.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        component1.SaveAs(path);
                        model.bpromo_image = UploadPath + fileName + extension;
                    }
                   
                }
                
                model.fpromo_sort = data.fpromo_sort;
                model.bpromo_url = data.bpromo_url;
                model.bpromo_sort = data.bpromo_sort;
                model.bpromo_status = data.bpromo_status;
                model.date_added = DateTime.Now;
                model.Insert();
            }
            else
            {
                promo_back currentpromo = promo_back.GetPromoById(data.bpromo_id);
                if (component1 != null && component1.ContentLength > 0)
                {
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/Management/assets/cmsimage/";
                    if (component1.ContentLength > 0)
                    {
                        extension = Path.GetExtension(component1.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        component1.SaveAs(path);
                        currentpromo.bpromo_image = UploadPath + fileName + extension;
                    }
                   
                }
                
                currentpromo.fpromo_sort = data.fpromo_sort;
                currentpromo.bpromo_url = data.bpromo_url;
                currentpromo.bpromo_sort = data.bpromo_sort;
                currentpromo.bpromo_status = data.bpromo_status;
                currentpromo.Update();
            }
            return RedirectToAction(MVC.Management.Promo.PromoItemList());
        }
        public virtual ActionResult Delete(int id)
        {
            if (id != 0)
            {
                promo_back datapromo = promo_back.GetPromoById(id);
                if (datapromo != null)
                {
                    datapromo.Delete();
                }
            }
            return RedirectToAction(MVC.Management.Promo.PromoItemList());
        }
    }
}