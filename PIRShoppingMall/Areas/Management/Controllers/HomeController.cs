﻿using PIRShoppingMall.common;
using PIRShoppingMall.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class HomeController : Controller
    {
        // GET: Management/Home
        public virtual ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public virtual ActionResult Login(LoginModel model)
        {
            if (!ValidateLogOn(model.Username, model.Password))
            {
                return View();
            }
            else {
                string pwd = PIRShoppingMall.common.Security.DataEncription.Encrypt(model.Password);
                data.user _User = data.user.GetByUsernamePassword(model.Username, pwd);
                if (_User != null)
                {
                    CreateAuthenticationCookie(_User);
                    Session["UserId"] = _User.user_name ;
                    return RedirectToAction(MVC.Management.Dashboard.LandingPage());
                }
                else
                {
                    ViewBag.Error = "Username atau password salah";
                }
            }
            return View();
        }
        private bool ValidateLogOn(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName))
            {
                ViewBag.ErrorUsername = "Username tidak boleh kosong";
            }

            if (String.IsNullOrEmpty(password))
            {
                @ViewBag.ErrorPassword = "Password tidak boleh kosong";
            }

            return ModelState.IsValid;
        }

        private void CreateAuthenticationCookie(data.user _User)
        {
            HttpCookie myCookie = new HttpCookie("PlazaindonesiaManagementCookie");
            if (!SiteSettings.Domain.Contains("localhost"))
            {
                myCookie.Domain = "." + SiteSettings.Domain + ".com";
            }

            myCookie["user"] = _User.user_id.ToString();
            myCookie.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(myCookie);
        }
        public virtual ActionResult LogOut()
        {
            Session["UserId"] = null;
            return RedirectToAction(MVC.Management.Home.Login());
        }
    }
}