﻿using PIRShoppingMall.Authorize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRShoppingMall.Areas.Management.Controllers
{

    public partial class PIFFManagementController : Controller
    {
        [PlazaindonesiaAuthorization]
        // GET: Management/PIFFManagement
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult PIFFHeader()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            data.IsActiveMenu isActive = data.IsActiveMenu.getIsActiveMenuByID("PIFF");
            ViewBag.PIFFActive = isActive.IsActive;
            return View();
        }
        [HttpPost]
        public virtual ActionResult TogglePIFFMenu(bool isToggle)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            data.IsActiveMenu isActive = data.IsActiveMenu.getIsActiveMenuByID("PIFF");
            try
            {
                if (isActive != null)
                {
                    isActive.IsActive = isToggle;
                    isActive.UpdatedDate = DateTime.Now;
                    isActive.UpdatedBy = Session["UserId"].ToString();
                    isActive.UpdateSave<data.IsActiveMenu>();
                }
                else
                {
                    data.IsActiveMenu active = new data.IsActiveMenu();
                    active.IsActiveMenuId = "PIFF";
                    active.IsActive = isToggle;
                    active.CreatedDate = DateTime.Now;
                    active.CreatedBy = Session["UserId"].ToString();
                    active.InsertSave<data.IsActiveMenu>();

                }
                return Json(isToggle, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                return Json("NotOK", JsonRequestBehavior.AllowGet);
            }
           
        }
        public virtual ActionResult SetHeader(string id)
        {
            if (Session["UserId"] == null)
            {
               return RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            data.PIFFHeader piffHeader = data.PIFFHeader.getPiffHeaderDataByID(id);
            if (piffHeader != null)
            {
                ViewData.Model = piffHeader;
                
            }
            else
            {
                piffHeader = new data.PIFFHeader();
                piffHeader.isActive = false;
                piffHeader.PiffHeaderID = id;
                ViewData.Model = piffHeader;
            }
          
            return View();
        }
        [HttpPost]
        public virtual ActionResult SetHeader(model.PIFFHeaderModel model,bool isActive)
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (!string.IsNullOrEmpty(model.PiffHeaderID))
            {
                data.PIFFHeader currentData = data.PIFFHeader.getPiffHeaderDataByID(model.PiffHeaderID);
                if (currentData != null)
                {
                    currentData.PiffTextHeader = model.PiffTextHeader;
                    currentData.PiffYoutubeLink = model.PiffYoutubeLink;
                    currentData.PiffTextDescription = model.PiffTextDescription;
                    currentData.UpdatedBy = Session["UserId"].ToString();
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.isActive = model.isActive;
                    currentData.UpdateSave<data.PIFFHeader>();
                }
                else
                {   
                    data.PIFFHeader newData = new data.PIFFHeader();
                    newData.PiffTextHeader = model.PiffTextHeader;
                    newData.PiffTextDescription = model.PiffTextDescription;
                    newData.PiffYoutubeLink = model.PiffYoutubeLink;
                    newData.CreatedBy = Session["UserId"].ToString();
                    newData.CreatedDate = DateTime.Now;
                    newData.PiffHeaderID = model.PiffHeaderID;
                    newData.isActive = model.isActive;
                    newData.InsertSave<data.PIFFHeader>();
                }
            }
            return RedirectToAction(MVC.Management.PIFFManagement.PIFFHeader());
        }
    }
}