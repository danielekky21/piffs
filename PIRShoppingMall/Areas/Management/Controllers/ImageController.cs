﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PIRShoppingMall.data;
using System.Web.Mvc;
using PIRShoppingMall.Authorize;

namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class ImageController : Controller
    {
        // GET: Management/Image
        [PlazaindonesiaAuthorization]
        public virtual ActionResult List()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewData.Model = data.image.GetAllActive().OrderBy(x => x.image_id).OrderByDescending(x => x.date_added).ToList();
            return View();
        }
        public virtual ActionResult AddEdit(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (id != 0)
            {
                data.image image = data.image.GetByID(id);
                ViewData.Model = image;
            }
            else
            {
                ViewData.Model = new data.image();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(image model)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (model.image_id != 0)
            {
                data.image imageModel = data.image.GetByID(model.image_id);
                if (!string.IsNullOrEmpty(model.image_image))
                {
                    imageModel.image_image = model.image_image;
                }
                imageModel.Update();
            }
            else
            {
                model.Insert();
            }
            return RedirectToAction(MVC.Management.Image.List());
        }
        public virtual ActionResult delete(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (id != 0)
            {
                data.image imageModel = data.image.GetByID(id);
                if (imageModel != null)
                {
                    imageModel.Delete();
                }
            }
            return RedirectToAction(MVC.Management.Image.List());
        }
    }
}