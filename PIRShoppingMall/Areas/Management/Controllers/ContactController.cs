﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;

namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class ContactController : Controller
    {
        // GET: Management/Contact
        public virtual ActionResult List()
        {
            ViewData.Model = ContactTb.GetAllContactTb().OrderByDescending(x => x.ContactId).ToList();
            return View();
        }
        public virtual ActionResult Detail(int id)
        {
            if (id != 0)
            {
                ContactTb data = ContactTb.GetContactByID(id);
                ViewData.Model = data;
            }
            return View();
        }
    }
}