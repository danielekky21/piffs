﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;
using PIRShoppingMall.Authorize;
using PIRShoppingMall.model;
using System.Configuration;

namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class BannerController : Controller
    {
        // GET: Management/Banner
        [PlazaindonesiaAuthorization]
        public virtual ActionResult List()
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction(MVC.Management.Home.Login());
            }
            ViewData.Model = data.banner.GetAllActive().OrderByDescending(x => x.status).ThenBy(x => x.banner_sort).ToList();
            return View();
        }
        public virtual ActionResult AddEdit(int id)
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction(MVC.Management.Home.Login());
            }
            ViewBag.imagelist = data.image.GetAllActive().OrderByDescending(x => x.date_added).ToList();
           
            if (id != 0)
            {
                data.banner banner = data.banner.GetByID(id);
                if (banner.banner_youtube != "")
                {
                    ViewBag.bannertype = "yt";
                }
                else
                {
                    ViewBag.bannertype = "image";
                }
                ViewData.Model = banner;
            }
            else
            {
                ViewData.Model = new data.banner();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(BannerModel model)
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction(MVC.Management.Home.Login());
            }
            if (model.banner_youtube == null)
            {
                model.banner_youtube = string.Empty;
            }
            if (model.banner_link == null)
            {
                model.banner_link = string.Empty;
            }
            if (model.bannerType == "yt")
            {
                model.image_id = null;
            }
            else
                if (model.bannerType == "image")
                {
                    model.banner_youtube = string.Empty;
                }
            if (model.banner_id != 0)
            {
                data.banner banner = data.banner.GetByID(model.banner_id);
                if (banner != null)
                {
                    banner.image_id = model.image_id;
                    banner.banner_youtube = model.banner_youtube;
                    banner.banner_link = model.banner_link;
                    banner.banner_sort = model.banner_sort;
                    banner.status = model.status;
                    if (model.isExtend)
                    {
                        banner.isExtend = model.isExtend;
                        banner.extend_title = model.extend_title;
                        banner.extend_description = model.extend_description;

                        if (model.extend_image > 0)
                        {
                            banner.extend_image = model.extend_image.ToString();
                            banner.extend_imagePath = image.GetImagePathByID(model.extend_image);

                        }
                        if (model.MiniGallery != null)
                        {
                            banner.miniGalleryImg = string.Empty;
                            foreach (var item in model.MiniGallery)
                            {
                                banner.miniGalleryImg = banner.miniGalleryImg + ";" + item;
                            }
                        }
                        else
                        {
                            banner.miniGalleryImg = null;
                        }
                    }
                    else
                    {
                        banner.isExtend = model.isExtend;
                    }
                }
                banner.Update();
            }
            else
            {
                data.banner newBanner = new banner();
                newBanner.image_id = model.image_id;
                newBanner.banner_youtube = model.banner_youtube;
                
                newBanner.banner_link = model.banner_link;
                newBanner.banner_sort = model.banner_sort;
                newBanner.status = model.status;
                if (model.isExtend)
                {
                    newBanner.isExtend = model.isExtend;
                    newBanner.extend_title = model.extend_title;
                    newBanner.extend_description = model.extend_description;
                   
                    if (model.extend_image != 0)
                    {
                         newBanner.extend_image = model.image_id.ToString();
                        newBanner.extend_imagePath = image.GetImagePathByID(model.extend_image);
                    }
                   
                }
              
                newBanner.Insert();
            }
            return RedirectToAction(MVC.Management.Banner.List());
        }
        public virtual ActionResult delete(int id)
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction(MVC.Management.Home.Login());
            }
            if (id != 0)
            {
                data.banner bannerModel = data.banner.GetByID(id);
                if (bannerModel != null)
                {
                    bannerModel.Delete();
                }
            }
            return RedirectToAction(MVC.Management.Banner.List());
        }
    }
}