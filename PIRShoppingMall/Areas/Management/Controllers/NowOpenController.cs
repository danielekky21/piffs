﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;

namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class NowOpenController : Controller
    {
        // GET: Management/NowOpen
        public virtual ActionResult Index()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            return View();
        }
        public virtual ActionResult List()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            ViewData.Model = opening.GetAllOpeningData().OrderByDescending(x => x.status).ThenBy(x => x.opening_sort).ToList();
            return View();
        }
        public virtual ActionResult AddEdit(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            List<SelectListItem> StatusItem = new List<SelectListItem>();
            ViewBag.imagelist = data.image.GetAllActive().OrderByDescending(x => x.date_added).ToList();
            StatusItem.Add(new SelectListItem { Text = "enable", Value = "1" });
            StatusItem.Add(new SelectListItem { Text = "disable", Value = "0" });

            ViewBag.Stat = StatusItem;
            if (id != 0)
            {
                ViewData.Model = opening.GetOpeningById(id);
            }
            else
            {
                ViewData.Model = new opening();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(opening model)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (model.opening_id != 0)
            {
                opening data = opening.GetOpeningById(model.opening_id);
                data.image_id = model.image_id;
                data.opening_title = model.opening_title;
                data.opening_floor = model.opening_floor;
                data.opening_desc = model.opening_desc;
                data.opening_sort = model.opening_sort;
                data.status = model.status;
                data.Update();
            }
            else
            {
                opening newData = new opening();
                newData.image_id = model.image_id;
                newData.opening_title = model.opening_title;
                newData.opening_floor = model.opening_floor;
                newData.opening_desc = model.opening_desc;
                newData.opening_sort = model.opening_sort;
                newData.status = model.status;
                newData.date_added = DateTime.Now;
                newData.Insert();
            }
            return RedirectToAction(MVC.Management.NowOpen.List());
        }
        public virtual ActionResult Delete(int id)
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            if (id != 0)
            {
                opening data = opening.GetOpeningById(id);
                if (data != null)
                {
                    data.Delete();
                }
            }
            return RedirectToAction(MVC.Management.NowOpen.List());
        }
    }
}