﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRShoppingMall.data;
using PIRShoppingMall.Authorize;
using PIRShoppingMall.data.Managed;

namespace PIRShoppingMall.Areas.Management.Controllers
{

    public partial class SubscribeController : Controller
    {
        // GET: Management/Subscribe
        [PlazaindonesiaAuthorization]
        public virtual ActionResult List()
        {
            ViewData.Model = Subsribe.GetAllSubscriber().OrderBy(x => x.CreatedDate).ToList();
            return View();
        }
    }
}