﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using PIRShoppingMall.data;
using System.Web.Mvc;

namespace PIRShoppingMall.Areas.Management.Controllers
{
    public partial class UploadController : Controller
    {
        // GET: Management/Upload
        public virtual ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public virtual JsonResult upload()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction(@Url.Action(MVC.Management.Home.Login()));
            }
            string fileName = "";
            string folder = "";
            string dir = "";
            string month = DateTime.Now.Month.ToString();
            string year = DateTime.Now.Year.ToString();
            string path = string.Empty;
            string UploadPath = string.Empty;
            string extension = string.Empty;

            foreach (string item in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[item] as HttpPostedFileBase;
                fileName = Guid.NewGuid().ToString();
                UploadPath = "/Areas/Management/assets/cmsimage/";
                //folder = month + "-" + year + "/";
                //dir = UploadPath + folder;
                //if (!Directory.Exists(dir))
                //{
                //    Directory.CreateDirectory(dir);
                //}

                if (file.ContentLength == 0)
                    continue;
                if (file.ContentLength > 0)
                {
                    extension = Path.GetExtension(file.FileName);
                    path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                    file.SaveAs(path);
                }

            }
            data.image images = new data.image();
            images.image_image = UploadPath + fileName + extension;
            images.Insert();
            return Json(UploadPath + fileName + extension, JsonRequestBehavior.AllowGet);

        }
    }
}