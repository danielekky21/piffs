﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.model
{
    public class PIFFShortFilm
    {
        public string Title_Film { get; set; }
        public string Title_Film_other { get; set; }
        public string date_completed { get; set; }
        public string Film_duration { get; set; }
        public string Film_Synopsis { get; set; }
        public string Biograph { get; set; }
        public string FilmFestival { get; set; }
        public string FilmFestivalNameDate { get; set; }
        public string Film_Director { get; set; }
        public string Film_Producer { get; set; }
        public string Film_ScriptWriter { get; set; }
        public string Film_DirectorOfPhoto { get; set; }
        public string Film_Cast { get; set; }
        public string Film_Editor { get; set; }
        public string Film_ArtDirector { get; set; }
        public string Film_MusicArrangement { get; set; }
        public string Film_Others { get; set; }
        public string Film_Award { get; set; }
        public string Reason { get; set; }
        public string Film_AwardNameDate { get; set; }
        public string Film_Format { get; set; }
        public string OtherFormat { get; set; }
        public string Color_Film { get; set; }
        public string Film_Sound { get; set; }
        public string Film_language { get; set; }
        public string Film_Subtitle { get; set; }
        public string Film_Link { get; set; }
    }
}
