﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace PIRShoppingMall.model
{
    public class NewsletterModel
    {
        [Required]
        public int newsletter_id { get; set; }
        [Required]
        public string newsletter_first_image { get; set; }
        [Required]
        public string newsletter_second_image { get; set; }
        [Required]
        public string newsletter_second_title { get; set; }
        [Required]
        public string newsletter_second_desc { get; set; }
        [Required]
        public string newsletter_second_url { get; set; }
        [Required]
        public string newsletter_third_image { get; set; }
        [Required]
        public string newsletter_fourth_image { get; set; }
        [Required]
        public string newsletter_fourth_desc { get; set; }
        [Required]
        public DateTime newsletter_date { get; set; }
        [Required]
        public byte status { get; set; }
        [Required]
        public DateTime date_added { get; set; }
    }
}
