﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.model
{
    public class privilegeContactModel
    {
        public string Name { get; set; }
        public string CardNumber { get; set; }
        public string MobilePhone { get; set; }
        public string DateOfBirth { get; set; }
        public string EmailAddress { get; set; }
        public string Description { get; set; }
    }
}
