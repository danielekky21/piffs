﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.model
{
    public class DropdownlistBinding
    {
        public int value { get; set; }
        public string text { get; set; }
    }
}
