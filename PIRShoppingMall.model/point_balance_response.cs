﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.model
{
    public class point_balance_response
    {
        public string base64string { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string FullName { get; set; }
        public string CardLevel { get; set; }
        public string CardNumber { get; set; }
        public string contactId { get; set; }
    }
}
