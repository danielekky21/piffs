﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.model
{
    public class piffDataModel
    {
        public int EventID { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string TypeEventID { get; set; }
        public string EventType { get; set; }
        public string EventName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int TotalSeat { get; set; }
        public int AvailableSeat { get; set; }
        public int TotalInvitation { get; set; }
        public int Confirmation { get; set; }
        public string isActive { get; set; }
        public string Action { get; set; }
    }
}
