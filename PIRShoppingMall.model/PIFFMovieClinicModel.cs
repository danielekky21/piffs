﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.model
{
    public class PIFFMovieClinicModel
    {
        public string Title_Film { get; set; }
        public string Title_Film_other { get; set; }
        public string date_completed { get; set; }
        public string Role { get; set; }
        public string Youtube_Link { get; set; }
        public string Award { get; set; }
        public string MovClinAward { get; set; }
        public string Education { get; set; }
        public string MajorGraduation { get; set; }
        public string MajorSmester { get; set; }
        public string Training { get; set; }
        public string TrainingName { get; set; }
        public string Reason { get; set; }
        public int  EventID { get; set; }
    }
}
