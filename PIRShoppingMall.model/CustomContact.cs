﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.model
{
    public class CustomContact
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string[] reciever { get; set; }
        public string message { get; set; }

    }
}
