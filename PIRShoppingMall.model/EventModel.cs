﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace PIRShoppingMall.model
{
    public class EventModel
    {
        [Required]
        public int event_id { get; set; }
        public int? event_image { get; set; }
        public string event_image_hover { get; set; }
        public string event_image_popup { get; set; }
        public string evnet_youtube { get; set; }
        public DateTime event_date { get; set; }
        public byte event_show_date { get; set; }
        public int event_sort { get; set; }
        public byte status { get;set; }
    }
}
