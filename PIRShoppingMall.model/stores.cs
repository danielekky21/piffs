﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.model
{
    public class stores
    {
        public string id_store { get; set; }
        public string id_ref_store_category {get;set;}
        public string id_ref_publish { get; set; }
        public string kav { get; set; }
        public string store { get; set; }
        public string description { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string image_1 { get; set; }
        public string image_2 { get; set; }
        public string image_promo { get; set; }
        public string image_peta { get; set; }
        public string kode { get; set; }
        public string lantai { get; set; }
        public string promo_link { get; set; }
        public string posisi_x { get; set; }
        public string posisi_y { get; set; }
    }
}
