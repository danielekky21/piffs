﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.model
{
    public class point_balance
    {
        public string contactId { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string FullName { get; set; }
        public string CardLevel { get; set; }
        public string CardNumber { get; set; }
    }
}
