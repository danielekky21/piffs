﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PIRShoppingMall.model
{
    public class BannerModel
    {
        [Required]
        public int banner_id { get; set; }
        [Required]
        public int? image_id { get; set; }
        public string banner_link { get; set; }
        public string banner_youtube { get; set; }
        [Required]
        public int banner_sort { get; set; }
        public byte status { get; set; }
        public DateTime date_added { get; set; }
        public string bannerType { get; set; }
        public bool isExtend { get; set; }
        public string extend_title { get; set; }
        [AllowHtml]
        public string extend_description { get; set; }
        public int extend_image { get; set; }
        public List<string> MiniGallery { get; set; }
    }
}
