﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.model
{
    public class PIFFRegistrationModel
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string UserMail { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string UserPassword { get; set; }
        public string Gender { get; set; }
    }
}
