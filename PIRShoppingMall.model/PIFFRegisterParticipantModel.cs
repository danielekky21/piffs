﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.model
{
    public class PIFFRegisterParticipantModel
    {
        public string EventCode { get; set; }
        public string EventID { get; set; }
        public string Amount { get; set; }
        public string ParticipantName { get; set; }
        public string ParticipantDoBirth { get; set; }
        public string ParticipantEmail { get; set; }
        public string ParticipantPhone { get; set; }
        public string ParticipantName2 { get; set; }
        public string ParticipantDoBirth2 { get; set; }
        public string ParticipantEmail2 { get; set; }
        public string ParticipantPhone2 { get; set; }
    }
}
