﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PIRShoppingMall.model
{
    public class PIFFHeaderModel
    {
        public string PiffHeaderID { get; set; }
        public string PiffYoutubeLink { get; set; }
        public string PiffTextHeader { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        [AllowHtml]
        public string PiffTextDescription { get; set; }
        public bool isActive { get; set; }
    }
}
