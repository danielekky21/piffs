﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace PIRShoppingMall.model
{
    public class cardMemberResponse
    {
        [JsonProperty("contact_id")]
        public string ContactId { get; set; }
        [JsonProperty("full_name")]
        public string FullName { get; set; }
        [JsonProperty("card_number")]
        public string CardNumber { get; set; }
        [JsonProperty("card_level")]
        public string CardLevel { get; set; }
        [JsonProperty("total_point")]
        public string TotalPoints { get; set; }
        [JsonProperty("next_expr_point")]
        public string NextExpiredPoint { get; set; }
        [JsonProperty("next_expr_date")]
        public string NextExpireDate { get; set; }
        [JsonProperty("last_point_earn")]
        public string LastPointearned { get; set; }
        [JsonProperty("last_point_earn_date")]
        public string LastPointEarnedDate { get; set; }
        [JsonProperty("_type")]
        public string _Type { get; set; }
    }
}
