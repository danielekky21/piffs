﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PIRShoppingMall.model
{
    public class BlogItem
    {
        [Required]
        public int blog_id { get; set; }
        public int image_id { get; set; }
        public string blog_title { get; set; }
        public string blog_desc { get; set; }
        public string blog_youtube { get; set; }
        public DateTime blog_date { get; set; }
        public int blog_sort { get; set; }
        public byte status { get; set; }
        public DateTime date_added { get; set; }
        public List<String> imageSlider { get; set; }
    }
}
