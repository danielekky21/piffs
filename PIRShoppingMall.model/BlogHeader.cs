﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PIRShoppingMall.model
{
    public class BlogHeader
    {
        [Required]
        public int other_blog_id { get; set; }
        public string blog_title { get; set; }
        public string blog_desc { get; set; }
        public DateTime blog_date { get; set; }
        public byte blog_status { get; set; }
    }
}
