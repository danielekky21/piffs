﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PIRShoppingMall.common
{
    public class GlobalFunction
    {
        public static int GetCookieAdminLogin()
        {
            int id = 0;

            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["PlazaindonesiaManagementCookie"].Values["user"]))
            {
                id = int.Parse(HttpContext.Current.Request.Cookies["PlazaindonesiaManagementCookie"].Values["user"]);
            }

            return id;
        }
        public static int ConvertMonthNameIntoInt(string month)
        {
            return DateTime.ParseExact(month, "MMMM",CultureInfo.InvariantCulture).Month;
        }
        public enum statusEnum { 
            disable = 0,
            enable = 1
        }
    }
}
