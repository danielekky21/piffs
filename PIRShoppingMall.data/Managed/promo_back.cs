﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class promo_back
    {
        public static IQueryable<promo_back> GetAllPromoItem()
        {
            return CurrentDataContext.CurrentContext.promo_back;
        }
        public static promo_back GetPromoById(int id)
        {
            return CurrentDataContext.CurrentContext.promo_back.FirstOrDefault(x => x.bpromo_id == id);
        }
        public static IQueryable<promo_back> GetPromoItemActive()
        {
            return CurrentDataContext.CurrentContext.promo_back.Where(x => x.bpromo_status == 1);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.InsertSave<promo_back>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<promo_back>();
            }
            return model;
        }
        public ResponseModel Delete()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Delete<promo_back>();
            }
            return model;
        }
    }
}
