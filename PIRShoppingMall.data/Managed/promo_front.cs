﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class promo_front
    {
        public static IQueryable<promo_front> GetAllPromoFront()
        {
            return CurrentDataContext.CurrentContext.promo_front;
        }
        public static promo_front GetPromoHeadByID(int id)
        {
            return CurrentDataContext.CurrentContext.promo_front.FirstOrDefault(x => x.fpromo_id == id);
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<promo_front>();
            }
            return model;
        }

    }
}
