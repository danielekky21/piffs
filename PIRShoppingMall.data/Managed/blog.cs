﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class blog
    {
        public static IQueryable<blog> GetAllBlogItem()
        {
            return CurrentDataContext.CurrentContext.blogs;
            
        }
        public static blog GetBlogItemByID(int id)
        {
            return CurrentDataContext.CurrentContext.blogs.FirstOrDefault(x => x.blog_id == id);
        }
        public static IQueryable<blog> GetAllBlogActive()
        {
            return CurrentDataContext.CurrentContext.blogs.Where(x => x.status == 1);
        }
        public static IQueryable<blog> GetBlogByMonthAndYear(int month,int year)
        {
            return CurrentDataContext.CurrentContext.blogs.Where(x => x.blog_date.Month == month && x.blog_date.Year == year);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.InsertSave<blog>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<blog>();
            }
            return model;
        }
        public ResponseModel Delete()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Delete<blog>();
            }
            return model;
        }
    }
}
