﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class user
    {
        public static user GetByUsernamePassword(string username, string password)
        {
            return CurrentDataContext.CurrentContext.users.FirstOrDefault(x => x.user_username == username && x.user_password == password);
        }
        public static user GetByID(int ID)
        {
            return CurrentDataContext.CurrentContext.users.FirstOrDefault(x => x.user_id == ID);
        }
        public static IQueryable<user> GetAllActiveUser()
        {
            return CurrentDataContext.CurrentContext.users.Where(x => x.status == 1);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.InsertSave<user>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<user>();
            }
            return model;
        }

    }
}
