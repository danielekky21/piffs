﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class FAQTb
    {
        public static IQueryable<FAQTb> GetAllFAQHeader()
        {
            return CurrentDataContext.CurrentContext.FAQTbs;
        }
        public static IQueryable<FAQTb> GetAllActiveFAQHeader()
        {
            return CurrentDataContext.CurrentContext.FAQTbs.Where(x => x.IsActive == true);
        }
        public static FAQTb GetFirstActiveFAQheader()
        {
            return CurrentDataContext.CurrentContext.FAQTbs.FirstOrDefault(x => x.IsActive == true);
        }
        public static FAQTb GetFAQByID(int id)
        {
            return CurrentDataContext.CurrentContext.FAQTbs.FirstOrDefault(x => x.FAQID == id);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.InsertSave<FAQTb>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<FAQTb>();
            }
            return model;
        }
    }
}
