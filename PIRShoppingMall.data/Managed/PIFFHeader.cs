﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.data
{
    public partial class PIFFHeader
    {
        public static PIFFHeader getPiffHeaderDataByID(string id)
        {
            return CurrentDataContext.CurrentContext.PIFFHeaders.FirstOrDefault(x => x.PiffHeaderID == id);
        }
        public static IQueryable<PIFFHeader> GetPIFFHeaderData()
        {
            return CurrentDataContext.CurrentContext.PIFFHeaders;
        }

        public static IQueryable<vw_MasterTypeEvent> GetPIFFMasterEventData()
        {
            return CurrentDataContext.CurrentContext.vw_MasterTypeEvent;
        }

        public static vw_MasterTypeEvent GetPIFFMasterEventDataID(int id)
        {
            return CurrentDataContext.CurrentContext.vw_MasterTypeEvent.FirstOrDefault(x => x.TypeEventID == id);
        }
    }
}
