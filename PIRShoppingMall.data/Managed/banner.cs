﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class banner
    {
        public static IQueryable<banner> GetAllActive()
        {
            return CurrentDataContext.CurrentContext.banners;
        }
        public static IQueryable<banner> GetOnlyActive()
        {
            return CurrentDataContext.CurrentContext.banners.Where(x => x.status == 1);
        }
        public static IQueryable<banner> GetIsExtend()
        {
            return CurrentDataContext.CurrentContext.banners.Where(x => x.status == 1 && x.isExtend == true);
        }
        public static banner GetByID(int id)
        {
            return CurrentDataContext.CurrentContext.banners.FirstOrDefault(x => x.banner_id == id);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.date_added = DateTime.Now;
                this.Save<banner>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.date_added = DateTime.Now;
                this.UpdateSave<banner>();
            }
            return model;
        }
        public ResponseModel Delete()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Delete<banner>();
            }
            return model;
        }
    }
}
