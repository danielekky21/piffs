﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.data
{
    public partial class IsActiveMenu
    {
        public static IsActiveMenu getIsActiveMenuByID(string id)
        {
            return CurrentDataContext.CurrentContext.IsActiveMenus.FirstOrDefault(x => x.IsActiveMenuId == id);
        }
    }
}
