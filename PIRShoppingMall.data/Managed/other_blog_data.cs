﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class other_blog_data
    {
        public static IQueryable<other_blog_data> GetBlogHeaderData()
        {
            return CurrentDataContext.CurrentContext.other_blog_data;
        }
        public static other_blog_data GetBlogHeaderActive()
        {
            return CurrentDataContext.CurrentContext.other_blog_data.FirstOrDefault(x => x.blog_status == 1);
        }
        public static other_blog_data GetBlogHeaderByID(int id)
        {
            return CurrentDataContext.CurrentContext.other_blog_data.FirstOrDefault(x => x.other_blog_id == id);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.InsertSave<other_blog_data>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<other_blog_data>();
            }
            return model;
        }
        public ResponseModel Delete()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Delete<other_blog_data>();
            }
            return model;
        }
        public static other_blog_data GetBlogHeaderByMonthAndYear(int month,int year)
        {
            return CurrentDataContext.CurrentContext.other_blog_data.FirstOrDefault(x => x.blog_date.Month == month && x.blog_date.Year == year);
        }
    }
}
