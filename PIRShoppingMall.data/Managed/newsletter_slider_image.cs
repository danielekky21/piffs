﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;
namespace PIRShoppingMall.data
{
    public partial class newsletter_slider_image
    {
        public static IQueryable<newsletter_slider_image> GetByNewsletterID(int id)
        {
            return CurrentDataContext.CurrentContext.newsletter_slider_image.Where(x => x.newsletter_id == id);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Save<newsletter_slider_image>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                
                this.UpdateSave<newsletter_slider_image>();
            }
            return model;
        }
        public ResponseModel Remove()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Delete<newsletter_slider_image>();
            }
            return model;
        }
        public static List<string> callImageId(List<newsletter_slider_image> items)
        {
            List<String> Output = new List<String>();
            foreach (var item in items)
            {
                Output.Add(item.image_id.ToString());

            }
            return Output;
        }
    }
}
