﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class ContactTb
    {
        public ResponseModel insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.InsertSave<ContactTb>();
            }
            return model;
        }
        public static IQueryable<ContactTb> GetAllContactTb()
        {
            return CurrentDataContext.CurrentContext.ContactTbs;
        }
        public static ContactTb GetContactByID(int id)
        {
            return CurrentDataContext.CurrentContext.ContactTbs.FirstOrDefault(x => x.ContactId == id);
        }
    }
    
}
