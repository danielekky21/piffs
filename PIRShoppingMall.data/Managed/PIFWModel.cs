﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.data.Managed
{
    public class PIFWModel
    {
        public string show { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string age { get; set; }
        public string phone { get; set; }
        public string pipc { get; set; }

    }
}
