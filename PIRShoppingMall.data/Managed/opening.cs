﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class opening
    {
        public static IQueryable<opening> GetAllOpeningData()
        {
            return CurrentDataContext.CurrentContext.openings;
        }
        public static opening GetOpeningById(int id)
        {
            return CurrentDataContext.CurrentContext.openings.FirstOrDefault(x => x.opening_id == id);
        }
        public static IQueryable<opening> GetAllActive()
        {
            return CurrentDataContext.CurrentContext.openings.Where(x => x.status == 1);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.InsertSave<opening>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<opening>();
            }
            return model;
        }
        public ResponseModel Delete()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Delete<opening>();
            }
            return model;
        }
    }
}
