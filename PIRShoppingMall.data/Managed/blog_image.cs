﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class blog_image
    {
        public static IQueryable<blog_image> GetByBlogID(int id)
        {
            return CurrentDataContext.CurrentContext.blog_image.Where(x => x.blog_id == id);
        }

        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Save<blog_image>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<blog_image>();
            }
            return model;
        }
        public ResponseModel Delete()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Delete<blog_image>();
            }
            return model;
        }
        public static List<string> callImageId(List<blog_image> items)
        {
            List<String> Output = new List<String>();
            foreach (var item in items)
            {
                Output.Add(item.image_id.ToString());
            }
            return Output;
        }
        public ResponseModel Remove()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Delete<blog_image>();
            }
            return model;
        }
    }
}
