﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.data;

namespace PIRShoppingMall.data.Managed
{
    public partial class Subsribe
    {
        public static IQueryable<Subscribe> GetAllSubscriber() {
            return CurrentDataContext.CurrentContext.Subscribes;
        }
    }
}
