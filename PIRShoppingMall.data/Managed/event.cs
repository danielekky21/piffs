﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;
namespace PIRShoppingMall.data
{
    public partial class @event
    {
        public static IQueryable<@event> GetAllActive()
        {
            return CurrentDataContext.CurrentContext.events;
        }
        public static IQueryable<@event> GetAllOnlyActive()
        {
            return CurrentDataContext.CurrentContext.events.Where(x => x.status == 1);
        }
        public static @event GetByID(int id)
        {
            return CurrentDataContext.CurrentContext.events.FirstOrDefault(x => x.event_id == id);
        }
        public static IQueryable<@event> GetEventByMonthAndYear(int month,int year)
        {
            return CurrentDataContext.CurrentContext.events.Where(x => x.event_date.Month == month && x.event_date.Year == year);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Save<@event>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<@event>();
            }
            return model;
        }
        public ResponseModel Delete()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Delete<@event>();
            }
            return model;
        }
    }
}
