﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class newsletter
    {
        public static IQueryable<newsletter> GetAllActive()
        {
            return CurrentDataContext.CurrentContext.newsletters;
        }
        public static newsletter GetByID(int id)
        {
            return CurrentDataContext.CurrentContext.newsletters.FirstOrDefault(x => x.newsletter_id == id);
        }
        public static newsletter GetFirstEnabledItem()
        {
            return CurrentDataContext.CurrentContext.newsletters.FirstOrDefault(x => x.status == 1);
        }
        public static newsletter GetNewsLetterByMonth(int month)
        {
            return CurrentDataContext.CurrentContext.newsletters.FirstOrDefault(x => x.newsletter_date.Month == month);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.date_added = DateTime.Now;
                this.Save<newsletter>();
            }
            return model;
        }
        public ResponseModel Update()
        { 
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<newsletter>();
            }
            return model;
        }
        public ResponseModel Delete()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Delete<newsletter>();
            }
            return model;
        }
    }
}
