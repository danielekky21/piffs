﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class image
    {
        public static IQueryable<image> GetAllActive()
        {
            return CurrentDataContext.CurrentContext.images.Where(x => x.status == 1);
        }
        public static image GetByID(int id)
        {
            return CurrentDataContext.CurrentContext.images.FirstOrDefault(x => x.image_id == id);
        }
        public static string GetImagePathByID(int id)
        {
            return CurrentDataContext.CurrentContext.images.FirstOrDefault(x => x.image_id == id).image_image;
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.date_added = DateTime.Now;
                this.date_modified = DateTime.Now;
                this.status = 1;
                this.Save<image>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {

                this.date_modified = DateTime.Now;
                this.UpdateSave<image>();
            }
            return model;
        }
        public ResponseModel Delete()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.Delete<image>();
            }
            return model;
        }
    }
}
