﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class Reward
    {
        public static IQueryable<Reward> GetAllActive()
        {
            return CurrentDataContext.CurrentContext.Rewards.Where(x => x.IsActive == true);
        }
        public static IQueryable<Reward> GetAll()
        {
            return CurrentDataContext.CurrentContext.Rewards;
        }
        public static Reward GetItemByID(int id)
        {
            return CurrentDataContext.CurrentContext.Rewards.FirstOrDefault(x => x.RewardID == id);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.InsertSave<Reward>();
            }
            return model;
        }
        public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<Reward>();
            }
            return model;
        }
    }
}
