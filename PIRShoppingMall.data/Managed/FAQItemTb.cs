﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRShoppingMall.model;

namespace PIRShoppingMall.data
{
    public partial class FAQItemTb
    {
        public static IQueryable<FAQItemTb> GetAllFAQItemList()
        {
            return CurrentDataContext.CurrentContext.FAQItemTbs;
        }
        public static IQueryable<FAQItemTb> GetAllFAQItem(int id)
        {
            return CurrentDataContext.CurrentContext.FAQItemTbs.Where(x => x.FAQID == id);
        }
        public static IQueryable<FAQItemTb> GetAllFAQItemActive(int id)
        {
            return CurrentDataContext.CurrentContext.FAQItemTbs.Where(x => x.IsActive == true && x.FAQID == id);
        }
        public static FAQItemTb GetFAQItemByID(int id,int faqid)
        {
            return CurrentDataContext.CurrentContext.FAQItemTbs.FirstOrDefault(x => x.FAQID == faqid);
        }
        public ResponseModel Insert()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.IsActive = true;
                this.CreatedDate = DateTime.Now;
                this.InsertSave<FAQItemTb>();
            }
            return model;
        }
         public ResponseModel Update()
        {
            ResponseModel model = new ResponseModel();
            if (model.Success)
            {
                this.UpdateSave<FAQItemTb>();
            }
            return model;
        }
    }
}
