﻿using PIRShoppingMall.data.GenericRepository;
namespace PIRShoppingMall.data.GenericRepository
{
    public interface IRepositoryFactory
    {
        IRepository Repository();
    }
}
