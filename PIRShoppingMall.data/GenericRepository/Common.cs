﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRShoppingMall.data
{
    public enum SortOrderType
    {
        Ascending,
        Descending
    }
}
