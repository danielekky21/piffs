﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using PIRShoppingMall.data;
using PIRShoppingMall.data.GenericRepository;

namespace PIRShoppingMall.data
{
    public static class CurrentDataContext
    {
        public static adminpir_plazaind_dbEntities1 CurrentContext
        {
            get
            {
                var repository = DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] as adminpir_plazaind_dbEntities1;//IRepository;
                if (repository == null)
                {

                    repository = new adminpir_plazaind_dbEntities1();
                    DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] = repository;
                }

                return repository;

            }
        }

        public static void CloseCurrentRepository()
        {
            var repository = DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] as adminpir_plazaind_dbEntities1;//IRepository;
            if (repository != null)
            {
                repository.Dispose();
                DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] = null;
            }
        }
    }
}
